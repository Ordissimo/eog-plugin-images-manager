/* EogImagesManager - Plugin.
 
   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "eog-images-manager-plugin.h"
#include "eog-images-manager-nav.h"
#include "eog-images-manager-store.h"
#include "eog-images-manager-toolbar.h"
#include "eog-utils.h"
#include "eog-images-manager-scheduler.h"
#include <eog-3.0/eog/eog-window-activatable.h>
#include <eog-3.0/eog/eog-debug.h>
#include <libpeas/peas.h>
#include <glib/gi18n.h>
#include <eog-3.0/eog/eog-thumb-view.h>
#include <eog-3.0/eog/eog-thumb-nav.h>
#include <eog-3.0/eog/eog-scroll-view.h>
#include <eog-3.0/eog/eog-application.h>

#include <libtracker-control/tracker-control.h>

#define EOG_CONF_UI		"org.gnome.eog.ui"
#define GALLERY_SIZE    "image-gallery-thumb-size"

enum {
    THUMBNAILS_SIZE_NORMAL = 90,
    THUMBNAILS_SIZE_MIDDLE = 173,
    THUMBNAILS_SIZE_LARGE = 256,
};

static void eog_window_activatable_iface_init (EogWindowActivatableInterface *iface);

struct _EogImagesManagerPluginPrivate {
    TrackerMinerManager *tracker_manager;

    GtkWidget *box_toolbar;
    GtkWidget *nav_manager;
    GtkWidget *view_manager;

    guint sortType;

    GtkWidget *area;
    gboolean throbber_visible;
    cairo_surface_t *throbber;

    guint size_throbber;
    GTimer *timer_throbber;
    gfloat throbber_x, throbber_y;
    gfloat middle_area_width, middle_area_height;

    guint text_size;
    gchar *test;
    gchar *text_display;
    gfloat text_x, text_y;

    GKeyFile *key_config;
    gchar *save_config;
    gchar *saved_folder;
    gchar *saved_image;
    gchar *name_saved_image; // The caption

    guint id_throbber;
    guint id_prepared;
    guint id_item_gallery;
    guint id_row_changed;
    guint id_row_deleted;

    GSettings *gallery_settings;
    gint size_thumbnail;

    GtkWidget *folder_info;

    gchar *no_images;
    GtkWidget *label_no_images;
  
    gboolean oinfo_exist;
};

G_DEFINE_DYNAMIC_TYPE_EXTENDED (EogImagesManagerPlugin,
                                eog_images_manager_plugin,
                                PEAS_TYPE_EXTENSION_BASE,
                                0,
                                G_ADD_PRIVATE_DYNAMIC(EogImagesManagerPlugin)
                                        G_IMPLEMENT_INTERFACE_DYNAMIC (EOG_TYPE_WINDOW_ACTIVATABLE,
                                                                       eog_window_activatable_iface_init))


enum {
    PROP_0,
    PROP_WINDOW
};

static void set_throbber_cb (EogImagesManagerNav *iconview, gboolean throbber_set, EogImagesManagerPlugin *plugin);

static void
eog_images_manager_plugin_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN (object);

    switch (prop_id)
    {
        case PROP_WINDOW:
            plugin->window = EOG_WINDOW (g_value_dup_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
eog_images_manager_plugin_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN (object);

    switch (prop_id)
    {
        case PROP_WINDOW:
            g_value_set_object (value, plugin->window);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
eog_images_manager_plugin_init (EogImagesManagerPlugin *plugin)
{
    eog_debug_message (DEBUG_PLUGINS, "EogImagesManagerPlugin initializing");
    plugin->priv = eog_images_manager_plugin_get_instance_private (plugin);

    plugin->priv->save_config = g_build_filename(g_get_user_config_dir(), "eog", "save.ini", NULL);
    plugin->priv->key_config = g_key_file_new();
    plugin->priv->saved_folder = NULL;
    plugin->priv->name_saved_image = NULL;
    plugin->priv->saved_image = NULL;

    plugin->priv->gallery_settings = g_settings_new (EOG_CONF_UI);
    plugin->priv->size_thumbnail = g_settings_get_enum(plugin->priv->gallery_settings, GALLERY_SIZE);

    plugin->priv->no_images = D_("No photos have been found.");

    if (!g_key_file_load_from_file(plugin->priv->key_config, plugin->priv->save_config, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, NULL)) {
        plugin->priv->sortType = 1;
    } else {
        plugin->priv->sortType = g_key_file_get_integer (plugin->priv->key_config, "Folder", "sort", NULL);
	if (!plugin->priv->sortType)
	  plugin->priv->sortType = 1;
        plugin->priv->saved_folder = g_key_file_get_string (plugin->priv->key_config, "Folder", "path", NULL);
        if (g_key_file_has_group(plugin->priv->key_config, "Image")) {
            plugin->priv->name_saved_image = g_key_file_get_string (plugin->priv->key_config, "Image", "name", NULL);
            plugin->priv->saved_image = g_key_file_get_string (plugin->priv->key_config, "Image", "path", NULL);
            g_settings_set_enum(plugin->priv->gallery_settings, GALLERY_SIZE, THUMBNAILS_SIZE_NORMAL);
        }
    }
    plugin->priv->throbber_visible = FALSE;

    plugin->priv->middle_area_width = 0;
    plugin->priv->middle_area_height = 0;
    plugin->priv->size_throbber = 30;

    plugin->priv->text_size = 20;
    plugin->priv->text_display = D_("Loading...");
    plugin->priv->test = D_("Close");
}

static void
eog_images_manager_plugin_dispose (GObject *object)
{
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN (object);
//    EogImagesManagerPluginPrivate *priv = plugin->priv;

    eog_debug_message (DEBUG_PLUGINS, "EogImagesManagerPlugin disposing");

    if (plugin->window != NULL) {
        g_object_unref (plugin->window);
        plugin->window = NULL;
    }

//    if (priv->view_manager != NULL) {
//        g_object_unref (priv->view_manager);
//        priv->view_manager = NULL;
//    }

//    if (priv->nav_manager != NULL) {
//        g_object_unref (priv->nav_manager);
//        priv->nav_manager = NULL;
//    }

    G_OBJECT_CLASS (eog_images_manager_plugin_parent_class)->dispose (object);
}

static gboolean
configure_area_cb (GtkWidget *widget,
                   GdkEvent  *event,
                   EogImagesManagerPlugin *plugin)
{
    g_return_val_if_fail (EOG_IS_IMAGES_MANAGER_PLUGIN(plugin), FALSE);
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    GtkAllocation alloc_area;
    gtk_widget_get_allocation(widget, &alloc_area);

    priv->middle_area_width = (gfloat)(alloc_area.width) / 2.f;
    priv->middle_area_height = (gfloat)(alloc_area.height) / 2.f ;

//        g_message ("AREA(%d, %d), HALF(%lf, %lf)", gtk_widget_get_allocated_width(box_next), gtk_widget_get_allocated_height(box_next), priv->middle_area_width, priv->middle_area_height);

    priv->throbber_x = priv->middle_area_width - priv->size_throbber / 2.f;
    priv->throbber_y = priv->middle_area_height - priv->size_throbber / 2.f;

    cairo_text_extents_t extents;
    cairo_t *cr = cairo_create(priv->throbber);
    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, priv->text_size);
    cairo_text_extents(cr, priv->text_display, &extents);
    priv->text_x = priv->middle_area_width - (extents.width / 2 - extents.x_bearing);
    priv->text_y = priv->middle_area_height + 25;
    cairo_destroy(cr);

    return TRUE;
}

static gboolean
redraw_area (gpointer data)
{
    gtk_widget_queue_draw(data);
    return G_SOURCE_REMOVE;
}

static gboolean
on_draw_event (GtkWidget    *widget,
               cairo_t *cr,
               EogImagesManagerPlugin      *plugin){

    g_return_val_if_fail(GTK_IS_DRAWING_AREA(widget), FALSE);

    EogImagesManagerPluginPrivate *priv = plugin->priv;

    gdouble el = g_timer_elapsed (priv->timer_throbber, NULL);
    gdouble r = (2 * G_PI) * el;

    cairo_set_source_rgba(cr, 0, 0, 0, 0.4);
    if( priv->oinfo_exist) {
      gint width = gtk_widget_get_allocated_width (priv->area);
      gint height = gtk_widget_get_allocated_height (priv->area);

      double radius =  10;
      double degrees = G_PI / 180.0;

      cairo_arc (cr, width - radius, radius, radius, -90 * degrees, 0 * degrees);
      cairo_arc (cr, width - radius, height - radius, radius, 0 * degrees, 90 * degrees);
      cairo_arc (cr, radius, height - radius, radius, 90 * degrees, 180 * degrees);
      cairo_arc (cr, radius, radius, radius, 180 * degrees, 270 * degrees);
      cairo_fill (cr);
    } else
      cairo_paint (cr);
    
    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, priv->text_size);
    cairo_move_to(cr, priv->text_x, priv->text_y);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_show_text(cr, priv->text_display);

    cairo_translate(cr, priv->throbber_x, priv->throbber_y);
    cairo_rotate(cr, r/(2*G_PI)*4.0);
    cairo_translate(cr, -(priv->middle_area_width - priv->throbber_x), -(priv->middle_area_height - priv->throbber_y));

    cairo_set_source_surface(cr, priv->throbber, 0, 0);
    cairo_paint (cr);

    g_idle_add (redraw_area, widget);
    return FALSE;
}

static gboolean
display_throbber (gpointer data)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_PLUGIN(data), G_SOURCE_REMOVE);
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN(data);
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    if (priv->throbber_visible) {
        if (!priv->timer_throbber)
            priv->timer_throbber = g_timer_new();
        else
            g_timer_start(priv->timer_throbber);

        gtk_widget_show(GTK_WIDGET(plugin->priv->area));

        gtk_widget_set_sensitive(GTK_WIDGET(priv->box_toolbar), FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(priv->nav_manager), FALSE);
    }
    return G_SOURCE_REMOVE;
}

static void
tracker_progress_cb (TrackerMinerManager *manager,
                     gchar               *miner,
                     gchar               *status,
                     gdouble              progress,
                     gint                 remaining_time,
                     gpointer             data) {
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN(data);
    EogImagesManagerPluginPrivate *priv = plugin->priv;
    
    if (!strcmp(status, "Idle")) {
        if (priv->throbber_visible) {
            priv->throbber_visible = FALSE;

            if (priv->timer_throbber)
                g_timer_stop(priv->timer_throbber);

            gtk_widget_set_sensitive(priv->box_toolbar, TRUE);
            gtk_widget_set_sensitive (priv->nav_manager, TRUE);
            gtk_widget_hide(priv->area);

            EogListStore *store = EOG_LIST_STORE(gtk_icon_view_get_model(GTK_ICON_VIEW(priv->view_manager)));

            if (store != NULL && eog_list_store_length(store) <= 0)
                eog_images_manager_nav_fill (EOG_IMAGES_MANAGER_NAV (priv->nav_manager));
        }
    } else if (!strcmp(status, "Processing…")){
        if (!priv->throbber_visible) {
            priv->throbber_visible = TRUE;

            if (!gtk_icon_view_get_model(GTK_ICON_VIEW(priv->view_manager))){
                GtkWidget *eog_view = eog_window_get_view (plugin->window);
                GtkWidget *box_info = eog_utils_get_widget_by_name (eog_view, "box-info");

                gtk_widget_set_sensitive(priv->box_toolbar, FALSE);
                gtk_widget_set_sensitive (priv->nav_manager, FALSE);
                if (box_info)
                    gtk_widget_hide(box_info);
                if (!priv->label_no_images) {
                    priv->label_no_images = gtk_label_new (priv->no_images);
                    GtkWidget *eog_overlay = gtk_grid_get_child_at(GTK_GRID (eog_view), 0, 0);
                    gtk_overlay_add_overlay(GTK_OVERLAY (eog_overlay), priv->label_no_images);
                }
                gtk_widget_show(priv->label_no_images);
                return;
            }
	    g_idle_add (display_throbber, data);
        }
    }

    return;
}

static void
tracker_activated_cb (TrackerMinerManager *manager,
                      gchar               *miner,
                      gpointer             user_data)
{
    if (!strcmp(miner, "org.freedesktop.Tracker1.Miner.Extract")
        || !strcmp(miner, "org.freedesktop.Tracker1.Miner.Applications"))
        return;

    gchar *status;
    gdouble prog;
    gint rt;

    if (tracker_miner_manager_get_status (manager, miner, &status, &prog, &rt)) {
        if (strcmp(status, "Processing…") == 0)
            tracker_progress_cb(manager, miner, status, prog, rt, user_data);
    }
}

static gboolean
resize_thumbnails_cb (gpointer data)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_PLUGIN(data), G_SOURCE_REMOVE);
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN(data);
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    EogThumbView *thumbview = EOG_THUMB_VIEW(eog_window_get_thumb_view(plugin->window));
    EogThumbNav *nav = EOG_THUMB_NAV(eog_window_get_thumb_nav(plugin->window));
    gint new_size = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
    eog_utils_resize_thumbnails(EOG_THUMB_NAV (nav), EOG_THUMB_VIEW(thumbview), new_size);

    if (priv->throbber_visible) {
        priv->throbber_visible = FALSE;

        if (priv->timer_throbber)
            g_timer_stop(priv->timer_throbber);

        gtk_widget_set_sensitive(priv->box_toolbar, TRUE);
        gtk_widget_set_sensitive(priv->nav_manager, TRUE);
        gtk_widget_hide(priv->area);
    }

    return G_SOURCE_REMOVE;
}

static gboolean
key_press_event_cb (GtkWidget *widget,
                    GdkEvent  *event,
                    gpointer   user_data)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_PLUGIN(user_data), FALSE);
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN(user_data);
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    EogThumbNav *nav = EOG_THUMB_NAV(eog_window_get_thumb_nav(plugin->window));
    EogThumbNavMode mode = eog_thumb_nav_get_mode(nav);

    if (mode != EOG_THUMB_NAV_MODE_MULTIPLE_ROWS || priv->throbber_visible)
        return FALSE;

    guint new_size;
    switch(event->key.keyval){
        case GDK_KEY_KP_Add:
        case GDK_KEY_plus:
            if ( (event->key.state & GDK_CONTROL_MASK)){
                new_size = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
                if (new_size < THUMBNAILS_SIZE_LARGE) {
                    if (!priv->throbber_visible) {
                        priv->throbber_visible = TRUE;
                        g_idle_add (display_throbber, plugin);
                    }

                    new_size = (new_size == THUMBNAILS_SIZE_NORMAL) ? THUMBNAILS_SIZE_MIDDLE : THUMBNAILS_SIZE_LARGE;
                    g_settings_set_enum(priv->gallery_settings, GALLERY_SIZE, new_size);
                    EogListStore *store = eog_window_get_store (plugin->window);
                    g_object_set (store, "size-thumbnail", new_size, NULL);

                    /*Timeout to let throbber appear*/
                    g_timeout_add (100, resize_thumbnails_cb, plugin);
                }
            }
            break;
        case GDK_KEY_KP_Subtract:
        case GDK_KEY_minus:
            if ( (event->key.state & GDK_CONTROL_MASK)) {
                new_size = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
                if (new_size > THUMBNAILS_SIZE_NORMAL) {
                    if (!priv->throbber_visible) {
                        priv->throbber_visible = TRUE;
                        g_idle_add (display_throbber, plugin);
                    }

                    new_size = (new_size == THUMBNAILS_SIZE_LARGE) ? THUMBNAILS_SIZE_MIDDLE : THUMBNAILS_SIZE_NORMAL;
                    g_settings_set_enum(priv->gallery_settings, GALLERY_SIZE, new_size);
                    EogListStore *store = eog_window_get_store (plugin->window);
                    g_object_set (store, "size-thumbnail", new_size, NULL);

                    /*Timeout to let throbber appear*/
                    g_timeout_add (100, resize_thumbnails_cb, plugin);
                }
            }
            break;
        case GDK_KEY_z:
            if ( (event->key.state & GDK_CONTROL_MASK)) {
	          GAction *undo_deleted = g_action_map_lookup_action (G_ACTION_MAP(plugin->window), "undo-delete");
		  g_action_activate (undo_deleted, NULL);
	    }
	    break;
        default:
            break;
    }
    return TRUE;
}

static void
close_manager (GtkWidget *widget,
               GdkEvent  *event,
               EogImagesManagerPlugin *plugin)
{
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    EogImage *img = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW(priv->view_manager));
    if (!img) {
        eog_window_close (plugin->window);
        return;
    }

    GtkTreeModel *model = gtk_icon_view_get_model (GTK_ICON_VIEW(priv->view_manager));
    gint pos = eog_list_store_get_pos_by_image(EOG_LIST_STORE(model), img);
    GtkTreePath *path = gtk_tree_path_new_from_indices(pos, -1);

    GtkTreeIter iter;
    gchar *folder = NULL;
    gtk_tree_model_get_iter(model, &iter, path);
    gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, FOLDER_KEY, &folder, -1);
    g_key_file_set_boolean (priv->key_config, "Eog", "from-args", FALSE);
    g_key_file_set_integer (priv->key_config, "Folder", "sort", eog_images_manager_nav_get_sort(EOG_IMAGES_MANAGER_NAV(priv->nav_manager)));
    g_key_file_set_string (priv->key_config, "Folder", "path", folder);

    EogImage *image = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW(eog_window_get_thumb_view(plugin->window)));
    if (image) {
        GFile *f = eog_image_get_file(image);
        g_key_file_set_string(priv->key_config, "Image", "name", eog_image_get_caption(image));
        g_key_file_set_string(priv->key_config, "Image", "path", g_file_get_uri(f));

        g_object_unref (image);
        g_object_unref (f);
    } else {
        g_key_file_remove_group (priv->key_config, "Image", NULL);
    }

    g_key_file_save_to_file (priv->key_config, priv->save_config, NULL);
    g_key_file_free(priv->key_config);

    eog_window_close (plugin->window);
}

static void
display_image_from_gallery (GtkIconView *eog_thumbview,
                            EogImagesManagerPlugin *plugin)
{
    EogImage *image = eog_thumb_view_get_first_selected_image (EOG_THUMB_VIEW (eog_thumbview));
    if (!image)
      return;

    GtkWidget *eog_nav = eog_window_get_thumb_nav (plugin->window);

    EogListStore *store = eog_window_get_store (plugin->window);
    gint pos = eog_list_store_get_pos_by_image (store, image);
    GtkTreePath *path = gtk_tree_path_new_from_indices (pos, -1);

    g_object_set(G_OBJECT (store), "size-thumbnail", THUMBNAILS_SIZE_NORMAL, NULL);
    eog_utils_resize_thumbnails(EOG_THUMB_NAV (eog_nav), EOG_THUMB_VIEW (eog_thumbview), THUMBNAILS_SIZE_NORMAL);
    eog_thumb_nav_set_mode (EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_ONE_ROW);

    gtk_icon_view_scroll_to_path (GTK_ICON_VIEW (eog_thumbview), path, TRUE, 0, 1);
    gtk_tree_path_free (path);
    return;
}

static void
orientation_changed_cb (GObject    *gobject,
                        GParamSpec *pspec,
                        gpointer    user_data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_PLUGIN(user_data));

    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN(user_data);
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    GtkWidget *eog_thumbview = eog_window_get_thumb_view(plugin->window);
    GtkWidget *eog_nav = eog_window_get_thumb_nav(plugin->window);
    EogThumbNavMode new_mode = eog_thumb_nav_get_mode (EOG_THUMB_NAV (eog_nav));

    gint len_images = eog_list_store_length (eog_window_get_store (plugin->window));

    if (new_mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS && len_images > 1) {
        eog_utils_set_mode_multiple_rows(plugin->window);
        priv->id_item_gallery = g_signal_connect (eog_thumbview, "selection-changed", G_CALLBACK(display_image_from_gallery), plugin);

	GtkWidget *toolbar = gtk_stack_get_child_by_name(GTK_STACK(plugin->stack_toolbars), "toolbar-manager");
        gtk_stack_set_visible_child(GTK_STACK(plugin->stack_toolbars), toolbar);

        gboolean set_items = (priv->sortType == SORTED_BY_FOLDERS_NAME) ? TRUE : FALSE;
        eog_images_manager_toolbar_hide_items(EOG_IMAGES_MANAGER_TOOLBAR(toolbar), TRUE, set_items);

	GtkWidget *eog_thumb_nav = eog_window_get_thumb_nav (plugin->window);
	GList *children = gtk_container_get_children (GTK_CONTAINER (eog_thumb_nav));
	gtk_widget_set_sensitive (GTK_WIDGET(g_list_first (children)->data), FALSE);
	g_list_free (children);

        g_signal_connect (plugin->window, "key-press-event", G_CALLBACK (key_press_event_cb), plugin);
    } else if (new_mode == EOG_THUMB_NAV_MODE_ONE_ROW) {
        if (priv->id_item_gallery != 0) {
            g_signal_handlers_disconnect_by_func(eog_thumbview, display_image_from_gallery, plugin);
            priv->id_item_gallery = 0;
        }

        eog_utils_set_mode_single_row(plugin->window);

        gtk_stack_set_visible_child_name(GTK_STACK(plugin->stack_toolbars), "main-toolbar");
        g_signal_handlers_disconnect_by_func(plugin->window, key_press_event_cb, plugin);

        eog_thumb_view_select_single(EOG_THUMB_VIEW (eog_thumbview), EOG_THUMB_VIEW_SELECT_CURRENT);
    }
}

static gboolean
load_throbber (gpointer data)
{
    EogImagesManagerPluginPrivate *priv = EOG_IMAGES_MANAGER_PLUGIN(data)->priv;

    if (!priv->id_prepared)
        return G_SOURCE_REMOVE;

    tracker_progress_cb (priv->tracker_manager,
                         NULL,
                         "Processing…",
                         0,
                         0,
                         data);

    priv->id_throbber = 0;

    return G_SOURCE_REMOVE;
}

static void
hide_throbber (GtkTreeModel *tree_model,
               GtkTreePath  *path,
               GtkTreeIter  *iter,
               EogImagesManagerPlugin *plugin)
{
    EogImage *img = NULL;
    gtk_tree_model_get (tree_model, iter, EOG_LIST_STORE_EOG_IMAGE, &img, -1);

    if (eog_image_get_thumbnail(img)) {
        EogImagesManagerPluginPrivate *priv = plugin->priv;
        tracker_progress_cb (priv->tracker_manager,
                             NULL,
                             "Idle",
                             0,
                             0,
                             plugin);
        g_signal_handlers_disconnect_by_func(tree_model, hide_throbber, plugin);
    }
    return;
}

static void
process_started (EogImagesManagerToolbar *toolbar,
                 gpointer   data)
{

    g_return_if_fail (EOG_IS_IMAGES_MANAGER_PLUGIN(data));

    EogImagesManagerPluginPrivate *priv = EOG_IMAGES_MANAGER_PLUGIN(data)->priv;
    gtk_widget_set_sensitive(GTK_WIDGET (data), FALSE);
    tracker_progress_cb (priv->tracker_manager,
                         NULL,
                         "Processing…",
                         0,
                         0,
                         data);
}

static void
process_stopped (EogImagesManagerToolbar *toolbar,
                 gpointer   data)
{

    g_return_if_fail (EOG_IS_IMAGES_MANAGER_PLUGIN(data));

    EogImagesManagerPluginPrivate *priv = EOG_IMAGES_MANAGER_PLUGIN(data)->priv;
    tracker_progress_cb (priv->tracker_manager,
                         NULL,
                         "Idle",
                         0,
                         0,
                         data);
}

void
empty_cb (GtkTreeModel *tree_model,
          GtkTreePath  *path,
          gpointer      user_data)
{
    if(eog_list_store_length (eog_window_get_store (EOG_IMAGES_MANAGER_PLUGIN (user_data)->window)))
        return;

    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN(user_data);
    EogImagesManagerPluginPrivate *priv = EOG_IMAGES_MANAGER_PLUGIN(user_data)->priv;

    if (!priv->label_no_images) {
        gtk_widget_set_sensitive(priv->box_toolbar, FALSE);
        gtk_widget_set_sensitive (priv->nav_manager, FALSE);

        priv->label_no_images = gtk_label_new (priv->no_images);
        GtkWidget *eog_view = eog_window_get_view (plugin->window);
        GtkWidget *eog_overlay = gtk_grid_get_child_at(GTK_GRID (eog_view), 0, 0);
        gtk_overlay_add_overlay(GTK_OVERLAY (eog_overlay), priv->label_no_images);
    }
    gtk_widget_show (priv->label_no_images);
    return;
}

static void
update_label_folder_cb (EogWindow *window, gpointer user_data)
{
  EogImagesManagerPluginPrivate *priv = EOG_IMAGES_MANAGER_PLUGIN(user_data)->priv;
  
  if (priv->id_prepared > 0) {
    g_signal_handler_disconnect(window, priv->id_prepared);
    priv->id_prepared = 0;
  }
  
  eog_images_manager_nav_update_label_folder (EOG_IMAGES_MANAGER_NAV(priv->nav_manager));

  GtkWidget *eog_nav = eog_window_get_thumb_nav (window);
  if (eog_thumb_nav_get_mode (EOG_THUMB_NAV (eog_nav)) == EOG_THUMB_NAV_MODE_ONE_ROW) {
    GtkWidget *eog_thumbview = eog_window_get_thumb_view (window);
    g_object_set(eog_window_get_store(window), "size-thumbnail", THUMBNAILS_SIZE_NORMAL, NULL);
    eog_utils_resize_thumbnails(EOG_THUMB_NAV (eog_nav), EOG_THUMB_VIEW (eog_thumbview), THUMBNAILS_SIZE_NORMAL);
    eog_thumb_view_select_single(EOG_THUMB_VIEW(eog_thumbview), EOG_THUMB_VIEW_SELECT_CURRENT);
  }
  return;
}

static void
update_manager_cb (GApplication *application,
		   GFile       **files,
		   gint          n_files,
		   gchar        *hint,
		   EogImagesManagerPlugin *plugin)
{
  GFile *file = (GFile*) files[n_files-1];
  g_return_if_fail (G_IS_FILE (file));
  
  eog_list_store_set_monitoring (TRUE);
  GFileInfo *file_info = g_file_query_info (file,
					    G_FILE_ATTRIBUTE_STANDARD_TYPE","
					    G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE","
					    G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
					    0, NULL, NULL);

  if (!file_info) {
    g_object_unref (file_info);
    g_object_unref (file);
    return;
  }
  GtkWidget *eog_nav = eog_window_get_thumb_nav(plugin->window);
  GFileType file_type = g_file_info_get_file_type (file_info);
  gchar *folder_path = NULL;
  if (file_type == G_FILE_TYPE_REGULAR) {
	    GFile *parent = g_file_get_parent (file);
	    folder_path = g_strdup(g_file_get_uri (parent));
	    if (eog_thumb_nav_get_mode (EOG_THUMB_NAV (eog_nav)) == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS)
	      eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_ONE_ROW);
  } else if (file_type == G_FILE_TYPE_DIRECTORY) {
    	    folder_path = g_strdup (g_file_get_uri (file));
    	    if (eog_thumb_nav_get_mode (EOG_THUMB_NAV (eog_nav)) == EOG_THUMB_NAV_MODE_ONE_ROW)
	      eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);
  }

  gchar *key = NULL;
  GtkTreeIter iter;
  EogImage *image = eog_thumb_view_get_first_selected_image (EOG_THUMB_VIEW (plugin->priv->view_manager));
  EogListStore *store = EOG_LIST_STORE(gtk_icon_view_get_model (GTK_ICON_VIEW (plugin->priv->view_manager)));
  gint pos = eog_list_store_get_pos_by_image (store, image);
  GtkTreePath *path = gtk_tree_path_new_from_indices (pos, -1);

  gtk_tree_model_get_iter (GTK_TREE_MODEL (store), &iter, path);

  gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, FOLDER_KEY, &key, -1);

  if (!strcmp (key, folder_path) && file_type == G_FILE_TYPE_REGULAR) {
    plugin->priv->id_prepared = g_signal_connect_after (plugin->window, "prepared", G_CALLBACK(update_label_folder_cb), plugin);
    g_free (folder_path);
    g_object_unref (file_info);
    return;
  }

  plugin->priv->id_prepared = g_signal_connect_after (plugin->window, "prepared", G_CALLBACK(update_label_folder_cb), plugin);

  eog_window_open_file_list(plugin->window, g_slist_append(NULL, g_object_ref(file)));
  eog_images_manager_nav_get_images_from_folder (EOG_IMAGES_MANAGER_NAV(plugin->priv->nav_manager), SORTED_BY_FOLDERS_NAME, folder_path);

  g_object_unref (file_info);
  if (folder_path)
    g_free (folder_path);
  return;
}

static gboolean
screen_orientation_changed_cb (GtkWidget *widget,
			       GdkEventConfigure  *event,
			       EogImagesManagerPlugin *plugin)
{
	gint w, h;
	GtkAllocation allocation;
	gtk_widget_get_allocation (widget, &allocation);
	w = allocation.width;
        h = allocation.height;

	GtkWidget *thumbnav = eog_images_manager_nav_get_thumb_nav (EOG_IMAGES_MANAGER_NAV (plugin->priv->nav_manager));
        GtkStyleContext *context = gtk_widget_get_style_context(plugin->priv->nav_manager);
	GtkOrientation orientation = gtk_orientable_get_orientation  (GTK_ORIENTABLE (plugin->hbox));
	if (w < h && orientation != GTK_ORIENTATION_VERTICAL) {
	        gtk_style_context_add_class(context, /* GTK_STYLE_CLASS_HORIZONTAL */"screen-horizontal");
		gtk_style_context_set_state (context, GTK_STATE_FLAG_ACTIVE);
		gtk_orientable_set_orientation (GTK_ORIENTABLE(plugin->hbox), GTK_ORIENTATION_VERTICAL);
		eog_thumb_nav_set_mode (EOG_THUMB_NAV (thumbnav), EOG_THUMB_NAV_MODE_ONE_ROW);
		eog_thumb_nav_set_show_buttons (EOG_THUMB_NAV (thumbnav), FALSE);
		gtk_widget_set_vexpand (plugin->priv->nav_manager, FALSE);
	} else if (w > h && orientation != GTK_ORIENTATION_HORIZONTAL) {
	        gtk_style_context_remove_class (context, "screen-horizontal");
		gtk_orientable_set_orientation (GTK_ORIENTABLE(plugin->hbox), GTK_ORIENTATION_HORIZONTAL);
		eog_thumb_nav_set_mode (EOG_THUMB_NAV (thumbnav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);
		eog_thumb_nav_set_show_buttons (EOG_THUMB_NAV (thumbnav), TRUE);
	    	GtkWidget *sw_view = gtk_widget_get_parent (plugin->priv->view_manager);
    		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(sw_view), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
		gtk_icon_view_set_columns(GTK_ICON_VIEW(plugin->priv->view_manager), 2);
		gtk_widget_set_vexpand (plugin->priv->nav_manager, TRUE);

	}

	return FALSE;
}

static void
window_loaded_cb (EogWindow *window,
                  EogImagesManagerPlugin *plugin)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_PLUGIN(plugin));
    EogImagesManagerPluginPrivate *priv = plugin->priv;

    GtkWidget *eog_thumbview = eog_window_get_thumb_view(plugin->window);
    GtkWidget *eog_nav = eog_window_get_thumb_nav(window);
    EogImage *img = NULL;
    g_settings_set_enum(plugin->priv->gallery_settings, GALLERY_SIZE, priv->size_thumbnail);
    if (!priv->saved_image) {
        gint len_images = eog_list_store_length (eog_window_get_store (plugin->window));
        eog_thumb_nav_set_mode(EOG_THUMB_NAV(eog_nav), len_images > 1 ? EOG_THUMB_NAV_MODE_MULTIPLE_ROWS : EOG_THUMB_NAV_MODE_ONE_ROW);
        if (len_images == 1){
            gtk_stack_set_visible_child_name(GTK_STACK(plugin->stack_toolbars), "main-toolbar");
        } else {
            g_idle_add(resize_thumbnails_cb, plugin);

            priv->id_item_gallery = g_signal_connect (eog_thumbview, "selection-changed", G_CALLBACK(display_image_from_gallery), plugin);
            GtkWidget *toolbar_manager = gtk_stack_get_child_by_name(GTK_STACK(plugin->stack_toolbars), "toolbar-manager");
            gtk_stack_set_visible_child (GTK_STACK(plugin->stack_toolbars), toolbar_manager);
            gtk_widget_show_all (toolbar_manager);
        }
    } else {
        GFile *f_image = g_file_new_for_uri(priv->saved_image);
        if (g_file_query_exists (f_image, NULL)) {
            img = eog_image_new_file(f_image, priv->name_saved_image);
            eog_thumb_view_set_current_image(EOG_THUMB_VIEW(eog_thumbview), img, TRUE);
            gtk_stack_set_visible_child_name(GTK_STACK(plugin->stack_toolbars), "main-toolbar");
        }
        g_object_unref (f_image);
        g_free(priv->saved_image);
        g_free(priv->name_saved_image);

        priv->saved_image = NULL;
        priv->name_saved_image = NULL;
    }

    GtkTreeModel *model = gtk_icon_view_get_model (GTK_ICON_VIEW (priv->view_manager));
    if (model)
      priv->id_row_deleted = g_signal_connect (model, "row-deleted", G_CALLBACK(empty_cb), plugin);

    GtkWidget *toolbar = gtk_stack_get_child_by_name(GTK_STACK(plugin->stack_toolbars), "toolbar-manager");
    gboolean set_items = (priv->sortType == SORTED_BY_FOLDERS_NAME) ? TRUE : FALSE;
    eog_images_manager_toolbar_hide_items(EOG_IMAGES_MANAGER_TOOLBAR(toolbar), TRUE, set_items);
    g_signal_connect (EOG_IMAGES_MANAGER_TOOLBAR(toolbar), "process-started", G_CALLBACK(process_started), plugin);
    g_signal_connect (EOG_IMAGES_MANAGER_TOOLBAR(toolbar), "process-stopped", G_CALLBACK(process_stopped), plugin);

    if (priv->id_prepared > 0) {
        g_signal_handler_disconnect(window, priv->id_prepared);
        priv->id_prepared = 0;
    }
    g_signal_connect (G_APPLICATION(EOG_APP), "open", G_CALLBACK(update_manager_cb), plugin);

    g_signal_connect (window, "configure-event", G_CALLBACK (screen_orientation_changed_cb), plugin);

    return;
}

static void
set_throbber_cb (EogImagesManagerNav *iconview, gboolean throbber_set, EogImagesManagerPlugin *plugin)
{
    if (throbber_set) {
        tracker_progress_cb (plugin->priv->tracker_manager,
                             NULL,
                             "Processing…",
                             0,
                             0,
                             plugin);
    } else {
        EogListStore *store = eog_window_get_store (plugin->window);
        if (store != NULL) {
		gtk_widget_set_sensitive (plugin->priv->nav_manager, TRUE);

            if (plugin->priv->label_no_images && gtk_widget_get_visible (plugin->priv->label_no_images)) {
                gtk_widget_destroy(plugin->priv->label_no_images);
                plugin->priv->label_no_images = NULL;
		gtk_widget_set_sensitive(plugin->priv->box_toolbar, TRUE);
            }

            GtkTreeModel *model_manager = gtk_icon_view_get_model (GTK_ICON_VIEW (plugin->priv->view_manager));
		        tracker_progress_cb (plugin->priv->tracker_manager,
                             NULL,
                             "Idle",
                             0,
                             0,
                             plugin);
            plugin->priv->id_row_deleted = g_signal_connect (model_manager, "row-deleted", G_CALLBACK(empty_cb), plugin);
        }
    }

    return;
}

gboolean
cancel_escape_cb (GtkWidget *widget,
		  GdkEventKey  *event,
		  EogImagesManagerPlugin *plugin)
{

  gboolean propagate = FALSE;
  GAction *action = NULL;
  const gchar *visible_filter = NULL;
  switch (event->keyval) {
  case GDK_KEY_Escape:
    visible_filter = gtk_stack_get_visible_child_name (GTK_STACK (plugin->stack_toolbars));
    if (strcmp (visible_filter, "main-toolbar") && strcmp (visible_filter, "toolbar-manager"))
      propagate = FALSE;
    else
      propagate = TRUE;
    break;
  case GDK_KEY_Down:
    action = g_action_map_lookup_action (G_ACTION_MAP(widget), "go-previous");
    g_action_activate (action, NULL);
    propagate = TRUE;
    break;
  case GDK_KEY_Up:
    action = g_action_map_lookup_action (G_ACTION_MAP(widget), "go-next");
    g_action_activate (action, NULL);
    propagate = TRUE;
    break;
  default:
    break;
  }
  return propagate;
}

static void
window_maximized (GObject    *gobject,
                GParamSpec *pspec,
                gpointer    user_data)
{
    g_return_if_fail (GTK_IS_WINDOW (gobject));
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_PLUGIN(user_data));

    g_idle_add (resize_thumbnails_cb, user_data);
}

static void
eog_images_manager_plugin_activate (EogWindowActivatable *activatable)
{
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN (activatable);
    EogImagesManagerPluginPrivate *priv = plugin->priv;
    EogWindow *window = plugin->window;

    gtk_window_set_default_size (GTK_WINDOW (window), 1024, 576);

    eog_images_manager_scheduler_init();

    GtkWidget *boxtool = eog_window_get_boxtool(window);
    priv->oinfo_exist = g_find_program_in_path ("oinfo") != NULL ? TRUE : FALSE;

    if (priv->oinfo_exist) {
      g_signal_connect (window, "key-press-event", G_CALLBACK (cancel_escape_cb), plugin);

      GtkWidget *img_close = gtk_image_new_from_icon_name("window-close", priv->oinfo_exist ? GTK_ICON_SIZE_DIALOG : GTK_ICON_SIZE_BUTTON);
      GtkWidget *button_close = gtk_button_new_with_label(priv->test);
      gtk_widget_set_name(button_close, "quit_button");
      gtk_button_set_image(GTK_BUTTON(button_close), img_close);
      gtk_button_set_always_show_image(GTK_BUTTON(button_close), TRUE);
      gtk_button_set_image_position(GTK_BUTTON(button_close), GTK_POS_TOP);

      gtk_box_pack_start(GTK_BOX(boxtool), button_close, FALSE, FALSE, 0);
      gtk_box_reorder_child(GTK_BOX(boxtool), button_close, 0);

      g_signal_connect (GTK_BUTTON(button_close), "button-release-event", G_CALLBACK(close_manager), plugin);
    } else {
      g_signal_connect (GTK_WINDOW(window), "notify::is-maximized", G_CALLBACK(window_maximized), plugin);
      g_signal_connect (GTK_WIDGET (window), "delete-event", G_CALLBACK(close_manager), plugin);
    }
    
    gtk_widget_show_all(boxtool);

    plugin->stack_toolbars = eog_utils_get_widget_by_name (boxtool, "stack_toolbar");
    if (!plugin->stack_toolbars) {
        plugin->stack_toolbars = gtk_stack_new();
        gtk_widget_set_name(plugin->stack_toolbars, "stack_toolbar");
    }

    priv->box_toolbar = eog_utils_get_widget_by_name (boxtool, "box_toolbar");
    if (!priv->box_toolbar) {
        priv->box_toolbar = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_widget_set_name (priv->box_toolbar, "box_toolbar");

        gtk_box_pack_start(GTK_BOX(priv->box_toolbar), plugin->stack_toolbars, TRUE, TRUE, 0);
        gtk_box_pack_start(GTK_BOX(boxtool), priv->box_toolbar, TRUE, TRUE, 0);
    }

    priv->view_manager = eog_thumb_view_new();
    GList *renderers = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (priv->view_manager));
    GtkCellRenderer *text_cell = renderers->next->data;
    g_object_set (text_cell, "visible", FALSE, NULL);
    g_list_free (renderers);

    gtk_icon_view_unset_model_drag_source (GTK_ICON_VIEW (priv->view_manager));
    gtk_icon_view_unset_model_drag_dest (GTK_ICON_VIEW (priv->view_manager));
    GError *err = NULL;
    gboolean from_eog = g_key_file_get_boolean (plugin->priv->key_config, "Eog", "from-args", &err);

    priv->nav_manager = eog_images_manager_nav_new(window, plugin->stack_toolbars, priv->view_manager, from_eog, priv->sortType, priv->saved_folder);
    g_signal_connect (EOG_IMAGES_MANAGER_NAV(priv->nav_manager), "images-loading", G_CALLBACK (set_throbber_cb), plugin);
    eog_images_manager_nav_fill (EOG_IMAGES_MANAGER_NAV(priv->nav_manager));
    gtk_widget_set_vexpand(priv->nav_manager, TRUE);
    gtk_widget_set_name(priv->nav_manager, "Manager");

    gtk_widget_show_all(priv->box_toolbar);

    eog_debug (DEBUG_PLUGINS);

    GtkWidget *eog_sidebar = eog_window_get_sidebar (window);
    if (gtk_widget_get_visible(eog_sidebar))
        gtk_widget_set_visible (eog_sidebar, FALSE);

    GtkWidget *eog_nav = eog_window_get_thumb_nav(window);

    GtkWidget *eog_thumbview = eog_window_get_thumb_view(window);
    gtk_widget_set_has_tooltip(GTK_WIDGET (eog_thumbview), FALSE);
    gtk_icon_view_unset_model_drag_source (GTK_ICON_VIEW (eog_thumbview));
    gtk_icon_view_unset_model_drag_dest (GTK_ICON_VIEW (eog_thumbview));
    g_object_set(eog_thumbview, "activate-on-single-click", TRUE, "margin", 9, "item-padding", 1, NULL);
    if (priv->size_thumbnail != THUMBNAILS_SIZE_NORMAL) {
        GList *cell_pix = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (eog_thumbview));
        gtk_cell_renderer_set_fixed_size(cell_pix->data, THUMBNAILS_SIZE_NORMAL, THUMBNAILS_SIZE_NORMAL);
    }

    GtkWidget *eog_scroll_view = eog_window_get_view(window);
    eog_scroll_view_set_use_bg_color(EOG_SCROLL_VIEW(eog_scroll_view), FALSE);
    GtkWidget *eog_overlay = gtk_grid_get_child_at (GTK_GRID (eog_scroll_view), 0, 0);
    if (!from_eog) {
      priv->label_no_images = gtk_label_new (priv->no_images);
      gtk_overlay_add_overlay (GTK_OVERLAY(eog_overlay), priv->label_no_images);
    }

    if (priv->oinfo_exist) {
      GtkWidget *eog_drawing_area = gtk_bin_get_child (GTK_BIN (eog_overlay));

      gtk_drag_source_unset (eog_drawing_area);
      guint id_motion_event;
      if (g_signal_parse_name ("motion-notify-event", GTK_TYPE_DRAWING_AREA, &id_motion_event, NULL, FALSE)) {
        guint motion_drag = g_signal_handler_find (eog_drawing_area, G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_DATA, id_motion_event, (GQuark) 0, NULL, NULL, eog_scroll_view);
        g_signal_handler_block (eog_drawing_area, motion_drag);

        g_signal_handlers_disconnect_matched (eog_drawing_area, G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_DATA | G_SIGNAL_MATCH_UNBLOCKED, id_motion_event, (GQuark) 0, NULL, NULL, eog_scroll_view);

        g_signal_handler_unblock (eog_drawing_area, motion_drag);
      }

      GList *children = gtk_container_get_children(GTK_CONTAINER (eog_overlay));
      GtkWidget *button_revealer = NULL;

      GtkWidget *revealer_left = g_list_nth_data(children, 1);
      button_revealer = gtk_bin_get_child (GTK_BIN(revealer_left));
      gtk_widget_set_has_tooltip(button_revealer, FALSE);

      GtkWidget *revealer_right = g_list_nth_data(children, 2);
      button_revealer = gtk_bin_get_child (GTK_BIN(revealer_right));
      gtk_widget_set_has_tooltip(button_revealer, FALSE);


      GtkWidget *image;
      image = eog_utils_get_widget_by_name(revealer_left, "GtkImage");
      g_object_set(image, "icon-size", GTK_ICON_SIZE_DIALOG, NULL);

      image = eog_utils_get_widget_by_name(revealer_right, "GtkImage");
      g_object_set(image, "icon-size", GTK_ICON_SIZE_DIALOG, NULL);

      gtk_widget_set_name (revealer_left, "revealer");
      gtk_widget_set_name (revealer_right, "revealer");

      gtk_revealer_set_reveal_child(GTK_REVEALER (revealer_left), TRUE);
      gtk_revealer_set_reveal_child(GTK_REVEALER (revealer_right), TRUE);

      gtk_widget_set_valign (revealer_left, GTK_ALIGN_FILL);
      gtk_widget_set_valign (revealer_right, GTK_ALIGN_FILL);
    }

    priv->tracker_manager = tracker_miner_manager_new_full (FALSE, NULL);
    g_signal_connect(priv->tracker_manager, "miner-activated", G_CALLBACK(tracker_activated_cb), plugin);
    g_signal_connect(priv->tracker_manager, "miner-progress", G_CALLBACK(tracker_progress_cb), plugin);

    GError *error = NULL;
    GdkPixbuf *pix = gdk_pixbuf_new_from_resource_at_scale("/org/gnome/eog/ui/icons/spinner.png", priv->size_throbber, priv->size_throbber, TRUE, &error);
    if (error || !pix){
        g_warning("(%s - %s) Could not load throbber : %s", __FILE__, __func__, error ? error->message : "");
        g_clear_error(&error);
    }

    priv->throbber = gdk_cairo_surface_create_from_pixbuf(pix, 0, NULL);
    if (cairo_surface_status(priv->throbber) != CAIRO_STATUS_SUCCESS)
        g_warning("(%s - %s) >Failed to create cairo_surface, status : %d", __FILE__, __func__, cairo_surface_status(plugin->priv->throbber));

    /*
     * This plugin and the plugin-toolbar create two GtkDrawingArea
     * TODO : Avoid creating this two widgets
     */
    GtkWidget *eog_hpaned = gtk_widget_get_parent (eog_sidebar);
    GtkWidget *overlay = gtk_overlay_new();
    priv->area = gtk_drawing_area_new();

    if (priv->oinfo_exist){
      gtk_widget_set_margin_start (priv->area, 20);
      gtk_widget_set_margin_end (priv->area, 10);
      gtk_widget_set_margin_bottom (priv->area, 10);
    }
    
    plugin->layout = gtk_widget_get_parent(eog_nav);
    gtk_container_remove (GTK_CONTAINER (plugin->layout), eog_hpaned);
    gtk_container_remove (GTK_CONTAINER (plugin->layout), eog_nav);

    priv->folder_info = gtk_label_new (NULL);
    gtk_label_set_xalign(GTK_LABEL (priv->folder_info), 0.01);
    plugin->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(plugin->vbox), priv->folder_info, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(plugin->vbox), eog_hpaned, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(plugin->vbox), eog_nav, FALSE, FALSE, 0);

    plugin->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(plugin->hbox), plugin->priv->nav_manager, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(plugin->hbox), overlay, TRUE, TRUE, 0);

    gtk_container_add (GTK_CONTAINER (plugin->layout), plugin->hbox);
    gtk_container_add(GTK_CONTAINER(overlay), plugin->vbox);

    gtk_overlay_add_overlay(GTK_OVERLAY(overlay), plugin->priv->area);

    gtk_widget_set_margin_top (plugin->vbox, 10);

    g_signal_connect (plugin->priv->area, "configure-event", G_CALLBACK(configure_area_cb), plugin);
    g_signal_connect(plugin->priv->area, "draw", G_CALLBACK(on_draw_event), plugin);

    gtk_widget_show_all (plugin->layout);
    gtk_widget_hide(plugin->priv->area);

    g_signal_connect (eog_nav, "notify::orientation", G_CALLBACK(orientation_changed_cb), plugin);
    priv->id_prepared = g_signal_connect_after (window, "prepared", G_CALLBACK(window_loaded_cb), plugin);

    priv->id_throbber = g_timeout_add (500, load_throbber, plugin); // if filling liststore take too long
}

static void
eog_images_manager_plugin_deactivate (EogWindowActivatable *activatable)
{
    EogImagesManagerPlugin *plugin = EOG_IMAGES_MANAGER_PLUGIN (activatable);
    EogWindow *window = plugin->window;

    GtkWidget *sidebar = eog_window_get_sidebar (window);
    GtkWidget *hpaned = gtk_widget_get_parent (sidebar);
    GtkWidget *nav = eog_window_get_thumb_nav(window);

    gtk_container_remove (GTK_CONTAINER(plugin->vbox), hpaned);
    gtk_container_remove (GTK_CONTAINER(plugin->vbox), nav);

    gtk_widget_destroy(plugin->hbox);

    g_assert(gtk_paned_get_child1(GTK_PANED(hpaned)) != NULL);
    g_assert(gtk_paned_get_child2(GTK_PANED(hpaned)) != NULL);

    /* The adress changed so we keep it like that */
    gtk_container_add (GTK_CONTAINER (plugin->layout), hpaned);
    gtk_container_add (GTK_CONTAINER (plugin->layout), nav);
}

static void
eog_images_manager_plugin_class_init (EogImagesManagerPluginClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->dispose = eog_images_manager_plugin_dispose;
    object_class->set_property = eog_images_manager_plugin_set_property;
    object_class->get_property = eog_images_manager_plugin_get_property;

    g_object_class_override_property (object_class, PROP_WINDOW, "window");

//    bind_textdomain_codeset (EIM_PACKAGE, "UTF-8");
//    textdomain (EIM_PACKAGE);
}

static void
eog_images_manager_plugin_class_finalize (EogImagesManagerPluginClass *klass)
{
}

static void
eog_window_activatable_iface_init (EogWindowActivatableInterface *iface)
{
    bindtextdomain (EIM_PACKAGE, EIM_LANG_DIR);
    bind_textdomain_codeset (EIM_PACKAGE, "UTF-8");
    //textdomain (EIM_PACKAGE);

    iface->activate = eog_images_manager_plugin_activate;
    iface->deactivate = eog_images_manager_plugin_deactivate;
}

G_MODULE_EXPORT void
peas_register_types (PeasObjectModule *module)
{
    eog_images_manager_plugin_register_type (G_TYPE_MODULE (module));
    peas_object_module_register_extension_type (module,
                                                EOG_TYPE_WINDOW_ACTIVATABLE,
                                                EOG_TYPE_IMAGES_MANAGER_PLUGIN);
}
