/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.
   Code based on Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#include "eog-images-manager-scheduler.h"
#include <eog-3.0/eog/eog-jobs.h>


/* sync thread tools */
static GCond  job_queue_cond;
static GMutex job_queue_mutex;

/* priority queues */
static GQueue queue_high   = G_QUEUE_INIT;
static GQueue queue_medium = G_QUEUE_INIT;
static GQueue queue_low    = G_QUEUE_INIT;

static GQueue *job_queue[EOG_JOB_N_PRIORITIES] = {
        &queue_high,
        &queue_medium,
        &queue_low
};

static void      eog_images_manager_scheduler_enqueue_job (EogJob         *job,
                                                EogJobPriority  priority);
static EogJob   *eog_images_manager_scheduler_dequeue_job (void);
static gpointer  eog_images_manager_scheduler             (gpointer        data);
static void      eog_job_process               (EogJob         *job);


static void
eog_images_manager_scheduler_enqueue_job (EogJob         *job,
                               EogJobPriority  priority)
{

    /* --- enter critical section --- */
    g_mutex_lock (&job_queue_mutex);

    g_queue_push_tail (job_queue[priority], job);
    g_cond_broadcast  (&job_queue_cond);

    /* --- leave critical section --- */
    g_mutex_unlock (&job_queue_mutex);
}

static EogJob *
eog_images_manager_scheduler_dequeue_job (void)
{
    EogJob *job;
    gint    priority;

    /* initialization */
    job = NULL;

    while (!job) {
        /* --- enter critical section --- */
        g_mutex_lock (&job_queue_mutex);

        /* try to retrieve the next job from priority queue */
        for (priority = EOG_JOB_PRIORITY_HIGH; priority < EOG_JOB_N_PRIORITIES; priority++) {
            job = (EogJob *) g_queue_pop_head (job_queue[priority]);

            if (job)
                break;
        }
        
        /* if there is no job, wait for it */
        if (!job) {
            g_cond_wait    (&job_queue_cond,
                            &job_queue_mutex);
            g_mutex_unlock (&job_queue_mutex);
            continue;
        }

        /* --- leave critical section --- */
        g_mutex_unlock (&job_queue_mutex);
    }

    return job;
}

static gpointer
eog_images_manager_scheduler (gpointer data)
{
    EogJob *job;

    while (TRUE) {
        /* retrieve the next job */
        job = eog_images_manager_scheduler_dequeue_job ();

        /* execute the job */
        eog_job_process (job);

        /* free executed job */
        g_object_unref  (job);
    }

    return NULL;
}

static void
eog_job_process (EogJob *job)
{
    g_return_if_fail (EOG_IS_JOB (job));

    /* nothing to do if job was cancelled */
    if (eog_job_is_cancelled (job))
        return;
    
    /* process the current job */
    eog_job_run (job);
}

void
eog_images_manager_scheduler_init ()
{
    g_thread_new ("EogImagesManagerScheduler",
                  eog_images_manager_scheduler,
                  NULL);
}

void
eog_images_manager_scheduler_add_job (EogJob *job)
{
    g_return_if_fail (EOG_IS_JOB (job));

    /* make sure the job isn't destroyed */
    g_object_ref (job);

    /* enqueue the job */
    eog_images_manager_scheduler_enqueue_job (job, EOG_JOB_PRIORITY_LOW);
}

void
eog_images_manager_scheduler_add_job_with_priority (EogJob         *job,
                                         EogJobPriority  priority)
{
    g_return_if_fail (EOG_IS_JOB (job));

    /* make sure the job isn't destroyed */
    g_object_ref (job);

    /* enqueue the job */
    eog_images_manager_scheduler_enqueue_job (job, priority);
}
