/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.
   Code based on Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_IMAGES_MANAGER_SCHEDULER_H
#define EOG_IMAGES_MANAGER_SCHEDULER_H

#include <eog-3.0/eog/eog-jobs.h>

G_BEGIN_DECLS

typedef enum {
    EOG_JOB_PRIORITY_HIGH,
    EOG_JOB_PRIORITY_MEDIUM,
    EOG_JOB_PRIORITY_LOW,
    EOG_JOB_N_PRIORITIES
} EogJobPriority;

/* initialization */
void eog_images_manager_scheduler_init                  (void);

/* jobs management */
void eog_images_manager_scheduler_add_job               (EogJob         *job);
void eog_images_manager_scheduler_add_job_with_priority (EogJob         *job,
                                                         EogJobPriority  priority);

G_END_DECLS

#endif //EOG_IMAGES_MANAGER_SCHEDULER_H
