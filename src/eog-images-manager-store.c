/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */


#include "eog-jobs-manager.h"
#include "eog-images-manager-store.h"
#include "eog-images-manager-scheduler.h"

#include <eog-3.0/eog/eog-image.h>
#include <eog-3.0/eog/eog-thumbnail.h>
#include <libtracker-sparql/tracker-sparql.h>
#include <glib/gi18n.h>

struct _EogImagesManagerStorePrivate{
    EogImagesManagerStoreSort  sort;

    TrackerNotifier             *notifier;
    GString                     *mimetypes;

    GSList                      *current_store;

    GDBusConnection             *dbus_con;
    guint                        dbus_signal;

    EogJob                      *job_manager;

    GdkPixbuf                   *frame;
    GdkPixbuf                   *busy_image;

    guint                        id_job_manager;
};

enum {
    SIG_FOLDERS_LOADED,
    SIG_FOLDER_UPDATED,
    SIG_LAST
};

guint signals[SIG_LAST];

enum {
    PROP_0,
    PROP_SORT,
};

G_DEFINE_TYPE_WITH_PRIVATE (EogImagesManagerStore, eog_images_manager_store, EOG_TYPE_LIST_STORE)

static void store_dbus_notifier_images (GDBusConnection *connection,
                                        const gchar     *sender_name,
                                        const gchar     *object_path,
                                        const gchar     *interface_name,
                                        const gchar     *signal_name,
                                        GVariant        *parameters,
                                        gpointer         user_data);

static void
eog_images_manager_store_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_images_manager_store_parent_class)->constructed)
        G_OBJECT_CLASS (eog_images_manager_store_parent_class)->constructed(object);

    EogImagesManagerStore *managerStore = EOG_IMAGES_MANAGER_STORE(object);

    if (managerStore->priv->sort == SORTED_BY_DATE)
        gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (managerStore),
                                              GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID,
                                              GTK_SORT_DESCENDING);

    g_object_set (object, "size-thumbnail", 90, NULL);
}

static void
eog_images_manager_store_dispose(GObject *object)
{
    EogImagesManagerStorePrivate *priv = EOG_IMAGES_MANAGER_STORE(object)->priv;

    if(priv->mimetypes != NULL){
        g_string_free(priv->mimetypes, TRUE);
        priv->mimetypes = NULL;
    }

    if(priv->busy_image != NULL) {
        g_object_unref (priv->busy_image);
        priv->busy_image = NULL;
    }

    if (priv->job_manager != NULL) {
        g_object_unref (priv->job_manager);
        priv->job_manager = NULL;
    }

    if (priv->current_store != NULL){
        g_slist_foreach (priv->current_store, (GFunc)g_free, NULL);
        g_slist_free (priv->current_store);
        priv->current_store = NULL;
    }

    g_dbus_connection_signal_unsubscribe (priv->dbus_con, priv->dbus_signal);

    G_OBJECT_CLASS(eog_images_manager_store_parent_class)->dispose(object);

    return;
}

static void
eog_images_manager_store_set_property (GObject          *object,
                                       guint             prop_id,
                                       const GValue     *value,
                                       GParamSpec       *pspec)
{
    EogImagesManagerStorePrivate *priv = EOG_IMAGES_MANAGER_STORE(object)->priv;

    switch (prop_id){
        case PROP_SORT:
            priv->sort = g_value_get_int(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_images_manager_store_class_init (EogImagesManagerStoreClass *class )
{
    GObjectClass *g_object_class = G_OBJECT_CLASS (class);
    g_object_class->constructed = eog_images_manager_store_constructed;
    g_object_class->dispose = eog_images_manager_store_dispose;
    g_object_class->set_property = eog_images_manager_store_set_property;

    signals[SIG_FOLDERS_LOADED] = g_signal_new("folders-loaded", G_TYPE_FROM_CLASS(class),
                                               G_SIGNAL_RUN_LAST,
                                               G_STRUCT_OFFSET(EogImagesManagerStoreClass, folders_loaded),
                                               NULL, NULL,
                                               g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIG_FOLDER_UPDATED] = g_signal_new("folder-updated", G_TYPE_FROM_CLASS(class),
                                               G_SIGNAL_RUN_LAST,
                                               G_STRUCT_OFFSET(EogImagesManagerStoreClass, folder_updated),
                                               NULL, NULL,
                                               g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    g_object_class_install_property(g_object_class, PROP_SORT,
                                    g_param_spec_int("sort", "", "",
                                                     SORTED_BY_FOLDERS_NAME, SORTED_BY_DATE, SORTED_BY_FOLDERS_NAME,
                                                     G_PARAM_WRITABLE |
                                                     G_PARAM_CONSTRUCT_ONLY|
                                                     G_PARAM_STATIC_STRINGS));
    return;
}

static GdkPixbuf *
eog_list_store_get_icon (const gchar *icon_name)
{
    GtkIconTheme *icon_theme;
    GdkPixbuf *pixbuf = NULL;

    icon_theme = gtk_icon_theme_get_default ();

    pixbuf = gtk_icon_theme_load_icon (icon_theme,
                                       icon_name,
                                       90,
                                       0,
                                       NULL);

    if (!pixbuf) {
        pixbuf = gtk_icon_theme_load_icon (icon_theme,
                                           "image-loading",
                                           90,
                                           0,
                                           NULL);
    }

    return pixbuf;
}
static void
get_updated_list_from_store (EogImagesManagerStore *store);
static gboolean
fill_store_cb (gpointer data);

static void
tracker_notifier_images (TrackerNotifier *self, GPtrArray *events, gpointer data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_STORE(data));
    EogImagesManagerStorePrivate *priv = EOG_IMAGES_MANAGER_STORE(data)->priv;
    /*
     * No need for the loop
     */
    TrackerNotifierEvent *event = g_ptr_array_index(events, (events->len-1));
    gint state_notifier = tracker_notifier_event_get_event_type(event);
    switch (state_notifier) {
        case TRACKER_NOTIFIER_EVENT_CREATE:/*
            if (priv->id_job_manager != 0) {
                g_source_remove(priv->id_job_manager);
                priv->id_job_manager = 0;
            }
            priv->id_job_manager = g_timeout_add(1000, fill_store_cb, data);*/
        fill_store_cb (data);
            break;
        case TRACKER_NOTIFIER_EVENT_DELETE:
            get_updated_list_from_store(EOG_IMAGES_MANAGER_STORE(data));
            eog_images_manager_store_remove_folder(EOG_IMAGES_MANAGER_STORE(data));
            break;
        case TRACKER_NOTIFIER_EVENT_UPDATE:
            if (priv->sort == SORTED_BY_DATE)
                g_signal_emit(data, signals[SIG_FOLDER_UPDATED], 0);
            break;
        default:
            break;
    }
    return;
}

static void
eog_images_manager_store_init (EogImagesManagerStore *store)
{
    store->priv = eog_images_manager_store_get_instance_private(store);
    EogImagesManagerStorePrivate *priv = store->priv;

    GType types[FOLDER_NUM_COLUMNS];
    types[EOG_LIST_STORE_THUMBNAIL] = GDK_TYPE_PIXBUF;
    types[EOG_LIST_STORE_EOG_IMAGE] = G_TYPE_OBJECT;
    types[EOG_LIST_STORE_THUMB_SET] = G_TYPE_BOOLEAN;
    types[EOG_LIST_STORE_EOG_JOB] = G_TYPE_POINTER;
    types[EOG_LIST_STORE_EOG_NAME] = G_TYPE_STRING;
    types[FOLDER_KEY] = G_TYPE_STRING;

    gtk_list_store_set_column_types (GTK_LIST_STORE (store),
                                     FOLDER_NUM_COLUMNS, types);

    /* For store to get images with the valid mimetypes */
    priv->mimetypes = g_string_new(NULL);
    GList *l_mimetype = eog_image_get_supported_mime_types();
    GList *it;
    for(it = l_mimetype; it != NULL; it = it->next)
        g_string_append_printf(priv->mimetypes, "'%s', ", (gchar*)it->data);

    // Temporary, we will add more mimetype (like psd)
    g_string_erase (priv->mimetypes, priv->mimetypes->len-2, 1);
/*
    gchar *type[2] = {"nfo:Image", NULL};
    priv->notifier = tracker_notifier_new((const gchar * const *)type, TRACKER_NOTIFIER_FLAG_NONE, NULL, NULL);
    g_signal_connect(TRACKER_NOTIFIER(priv->notifier), "events", G_CALLBACK(tracker_notifier_images), store);

*/
    priv->dbus_con = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, NULL);
    priv->dbus_signal = g_dbus_connection_signal_subscribe (priv->dbus_con,
                                                            TRACKER_DBUS_SERVICE,
                                                            TRACKER_DBUS_INTERFACE_RESOURCES,
                                                            "GraphUpdated",
                                                            TRACKER_DBUS_OBJECT_RESOURCES,
                                                            "http://www.semanticdesktop.org/ontologies/2007/03/22/nfo#Image",
                                                            G_DBUS_SIGNAL_FLAGS_NONE,
                                                            store_dbus_notifier_images,
                                                            store,
                                                            NULL);

    priv->current_store = NULL;
    priv->id_job_manager = 0;
    priv->frame = gdk_pixbuf_new_from_resource("/org/gnome/eog/ui/pixmaps/frame.png", NULL);
    priv->busy_image = eog_list_store_get_icon ("folder-loading");
}


GtkListStore*
eog_images_manager_store_new (EogImagesManagerStoreSort sort)
{
    return g_object_new(EOG_TYPE_IMAGES_MANAGER_STORE, "sort", sort, NULL);
}

/* * * * * *  * * * * * * * * * * *
 *
 * UTILS METHOD
 *
 * * * * * *  * * * * * * * *  * * */
static gboolean
is_folder_in_store (GtkTreeModel *model, const gchar *folder_key)
{
    gboolean found = FALSE;
    GtkTreeIter iter;
    gchar *key;

    if (!gtk_tree_model_get_iter_first (model, &iter))
        return found;

    do{
        gtk_tree_model_get(model, &iter, FOLDER_KEY, &key, -1);
        found = (strcmp(key, folder_key) == 0) ? TRUE : FALSE;
        g_free(key);
    } while (!found && gtk_tree_model_iter_next(model, &iter));

    return found;
}

static gboolean
is_folder_in_list (GSList *list, const gchar *folder_key)
{
    GSList *it;
    gchar *key;
    for (it = list; it != NULL; it = it->next) {
        key = (gchar *)it->data;
        if (!strcmp(key, folder_key))
            return TRUE;
    }
    return FALSE;
}

static void
get_updated_list_from_store (EogImagesManagerStore *store)
{
    GError *error = NULL;
    TrackerSparqlConnection *connection = tracker_sparql_connection_get(NULL, &error);
    if (!connection) {
        g_printerr("Couldn't obtain a direct connection to the Store store : %s\n",
                   error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    EogImagesManagerStorePrivate *priv = store->priv;

    gchar *query = NULL;
    if (priv->sort == SORTED_BY_FOLDERS_NAME)

        query = g_strdup_printf("SELECT ?urlfolder WHERE { "
                                "?urn a nfo:Image ; nie:url ?name ; nie:mimeType ?mimetype ; nfo:belongsToContainer ?folder . "
                                "?folder nie:url ?urlfolder . "
                                "FILTER (?mimetype IN (%s))"
                                "}GROUP BY ?folder ORDER BY ASC(?foldername)", priv->mimetypes->str);
    else if (priv->sort == SORTED_BY_DATE)

        query = g_strdup_printf("SELECT ?ym WHERE{ "
                                "?img a nfo:Image ; nie:mimeType ?mimetype . "
                                "BIND(tracker:coalesce(nie:contentCreated(?img), nfo:fileLastModified(?img)) AS ?date) "
                                "BIND(SUBSTR(?date, 1, 7) AS ?ym) . "
                                "FILTER (?mimetype IN (%s)) "
                                "} GROUP BY ?ym" , priv->mimetypes->str);

    TrackerSparqlCursor *cursor = tracker_sparql_connection_query(connection, query, NULL, &error);
    if (error) {
        g_printerr("Couldn't query the Store store : %s\n", error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    if (!cursor) {
        g_printerr("Couldn't found result matching the query : %s\n", query);
        return;
    }

    if (priv->current_store != NULL) {
        g_slist_foreach (priv->current_store, (GFunc)g_free, NULL);
        g_slist_free (priv->current_store);
        priv->current_store = NULL;
    }

    while (tracker_sparql_cursor_next(cursor, NULL, &error)) {
        const gchar *path_folder = tracker_sparql_cursor_get_string(cursor, 0, NULL);
        priv->current_store = g_slist_prepend(priv->current_store, g_strdup(path_folder));
    }

    g_object_unref (cursor);
    g_object_unref (connection);
    g_free (query);
    return;
}

/* * * * * *  * * * * * * * * * * *
 *
 * JOBS SIGNALS
 *
 * * * * * *  * * * * * * * *  * * */
static void
eog_job_manager_load_cb (EogJobManager *job, gpointer data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_STORE(data));
    EogImagesManagerStorePrivate *priv = EOG_IMAGES_MANAGER_STORE(data)->priv;

    if (job->folder != NULL) {
        GSList *it;
        for (it = job->folder; it != NULL; it = it->next) {
            folderContent *folder = (folderContent*)it->data;
            eog_images_manager_store_add_folder(EOG_IMAGES_MANAGER_STORE(data), folder->path_folder, folder->folder_name, folder->icon_folder, folder->icon_name);
        }
        priv->current_store = g_slist_concat(job->file_list, priv->current_store);
    }
    if (priv->sort != SORTED_BY_DATE)
        g_signal_emit(data, signals[SIG_FOLDER_UPDATED], 0);

    g_object_unref (priv->job_manager);
    priv->job_manager = NULL;

    // TEMPORARY SOLUTION
    get_updated_list_from_store(EOG_IMAGES_MANAGER_STORE(data));
    eog_images_manager_store_remove_folder(EOG_IMAGES_MANAGER_STORE(data));

    g_signal_emit(data, signals[SIG_FOLDERS_LOADED], 0);
    return;
}

/*
 * Add our frame and add text on pixbuf
 * Fixme: Not the better way
 */
static void
on_image_changed (EogImage *image, gchar *folder_name)
{
    GError *error = NULL;
    GdkPixbuf *pix = eog_thumbnail_load(image, &error);

    gdouble width = gdk_pixbuf_get_width(pix);
    gdouble height = gdk_pixbuf_get_height(pix);

    gint dimension = 90;
    gfloat factor = 1;
    if (height < dimension && height < width)
        factor = (gfloat) dimension / (gfloat) height;
    else if (width < dimension)
        factor = (gfloat) dimension / (gfloat) width;

    width  = MAX (width  * factor, 1);
    height =  MAX (height * factor, 1);

    GdkPixbuf *scaled_pixbuf = gdk_pixbuf_scale_simple (pix, width, height, GDK_INTERP_BILINEAR);

    gint center_x = width / 2;
    gint center_y = height / 2;

    gint coord_x = center_x - (dimension / 2) < 0 ? 0 : center_x - (dimension / 2);
    gint coord_y = center_y - (dimension / 2) < 0 ? 0 : center_y - (dimension / 2);

    gint width_area = width < dimension ? width : dimension;
    gint height_area = height < dimension ? height : dimension;

    GdkPixbuf *result_pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                                               TRUE, 8,
                                               dimension, dimension);

    gdk_pixbuf_fill (result_pixbuf, 0xffffffff);

    gdk_pixbuf_copy_area(scaled_pixbuf, coord_x, coord_y, width_area, height_area, result_pixbuf, 0, 0);

    GdkPixbuf *pix_frame = gdk_pixbuf_new_from_resource("/org/gnome/eog/ui/pixmaps/frame.png", NULL);
    cairo_surface_t *thumb = gdk_cairo_surface_create_from_pixbuf(result_pixbuf, 0, NULL);
    cairo_surface_t *frame = gdk_cairo_surface_create_from_pixbuf(pix_frame, 0, NULL);

    cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width_area, height_area);
    cairo_t *cr = cairo_create(surface);

    width = height = dimension;
    cairo_set_source_surface (cr, thumb, 0, 0);
    cairo_paint(cr);

    cairo_set_operator(cr,CAIRO_OPERATOR_DEST_OUT);

    cairo_set_source_surface (cr, frame, 0, 0);
    cairo_paint(cr);

    cairo_surface_flush(surface);

    cairo_set_operator(cr,CAIRO_OPERATOR_OVER);

    cairo_set_source_rgba (cr, 0, 0, 0, 0.4);
    cairo_rectangle (cr, 0, (height / 2) - 10, width, 25);
    cairo_fill(cr);

    cairo_set_source_rgb(cr, 1, 1, 1);
    PangoLayout *layout = pango_cairo_create_layout (cr);
    PangoFontDescription *desc = pango_font_description_from_string ("Helvetica Normal Bold 10");
    pango_layout_set_text (layout, folder_name, -1);
    pango_layout_set_font_description (layout, desc);
    pango_font_description_free (desc);
    pango_layout_set_ellipsize (layout, PANGO_ELLIPSIZE_END);
    pango_layout_set_width(layout, (gint)(width - 5) * PANGO_SCALE);

    pango_cairo_update_layout (cr, layout);

    pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
    cairo_move_to(cr, (width / PANGO_SCALE) / 2, (height / 2) - 8);
    pango_cairo_show_layout (cr, layout);

    GdkPixbuf *cairo_pix = gdk_pixbuf_get_from_surface(surface, 0, 0, width, height);
    // it emits the signal thumbnail-changed so we block it
    g_signal_handlers_block_by_func(image, on_image_changed, folder_name);
    eog_image_set_thumbnail(image, cairo_pix);
    g_signal_handlers_unblock_by_func(image, on_image_changed, folder_name);

    g_object_unref (pix_frame);
    cairo_surface_destroy(surface);
    cairo_surface_destroy(thumb);
    cairo_surface_destroy(frame);
    cairo_destroy(cr);
}
/**
 * eog_images_manager_store_add_folder:
 * @store: A #EogImagesManagerStore
 * @path_folder: An uri of path of the folder containing images
 * @foldername: The name for the folder
 * @icon_folder: The icon for the folder
 * @icon_name: The name of the icon (The icon is the first image, so we must add the caption)
 */

void
eog_images_manager_store_add_folder (EogImagesManagerStore *store,
                                     const gchar *path_folder,
                                     const gchar *foldername,
                                     const gchar *icon_folder,
                                     const gchar *icon_name)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_STORE(store));

    //if (!is_folder_in_store(GTK_TREE_MODEL (store), path_folder)) {
        GtkTreeIter iter;
        EogImage *image = eog_image_new_file(g_file_new_for_uri(icon_folder), icon_name);

        g_signal_connect (image, "thumbnail-changed",
                          G_CALLBACK (on_image_changed),
                          g_strdup(foldername));

        gtk_list_store_append(GTK_LIST_STORE(store), &iter);
        gtk_list_store_set(GTK_LIST_STORE(store), &iter,
                           EOG_LIST_STORE_EOG_IMAGE, image,
                           EOG_LIST_STORE_THUMBNAIL, store->priv->busy_image,
                           EOG_LIST_STORE_THUMB_SET, FALSE,
//                           EOG_LIST_STORE_EOG_NAME, g_strdup(foldername),
                           FOLDER_KEY, path_folder, -1);

///    }
    return;
}

/**
 * remove_folder_in_list_store:
 * @store: A #EogImagesManagerStore
 */
void
eog_images_manager_store_remove_folder (EogImagesManagerStore *store)
{
    g_return_if_fail(GTK_IS_TREE_MODEL(store));

    EogImagesManagerStorePrivate *priv = store->priv;

    GtkTreeIter iter;
    gboolean valid =  gtk_tree_model_get_iter_first (GTK_TREE_MODEL(store), &iter);
    if(!valid) return;

    gboolean exist;

    EogImage *image = NULL;
    gchar *key = NULL;
    gchar *name = NULL;

    while(valid) {
        if (key)
            g_free (key);
        if (name)
            g_free(name);


        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter, EOG_LIST_STORE_EOG_IMAGE, &image, FOLDER_KEY, &key, /*FOLDER_NAME*/EOG_LIST_STORE_EOG_NAME, &name, -1);

        exist = is_folder_in_list(priv->current_store, key);

        if(!exist){
            g_object_unref (image);
            valid = gtk_list_store_remove(GTK_LIST_STORE(store), &iter);
            if (valid) {
                if (priv->current_store)
                    priv->current_store = g_slist_remove(priv->current_store, key);
                /* *
                 *
                 * Avoid to iterate again because gtk_list_store_remove go to the next iter
                 * That mean if we remove two folders side by side and gtk remove the first, the iter will skip the second
                 *
                 * */
                continue;
            }
            break;
        }
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
    }

    return;
}

static gboolean
fill_store_cb (gpointer data)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_STORE(data), G_SOURCE_REMOVE);

    EogImagesManagerStorePrivate *priv = EOG_IMAGES_MANAGER_STORE(data)->priv;
    priv->id_job_manager = 0;

    if (priv->job_manager != NULL) {
        return G_SOURCE_REMOVE;
    }

    GString* string_path = g_string_new(NULL);
    GSList* it;
    for (it = priv->current_store; it != NULL; it = it->next) {
        if (it->next == NULL)
            g_string_append_printf(string_path, "\"%s\"", (gchar*)it->data);
        else
            g_string_append_printf(string_path, "\"%s\", ", (gchar*)it->data);
    }

    g_assert (string_path != NULL);

    priv->job_manager = eog_job_manager_new(priv->sort, EOG_LIST_STORE(data), string_path->str, priv->mimetypes->str);
    g_signal_connect (priv->job_manager,"finished", G_CALLBACK (eog_job_manager_load_cb), data);
    eog_images_manager_scheduler_add_job(priv->job_manager);
    g_string_free (string_path, TRUE);
    return G_SOURCE_REMOVE;
}

static void
store_dbus_notifier_images (GDBusConnection *connection,
                            const gchar     *sender_name,
                            const gchar     *object_path,
                            const gchar     *interface_name,
                            const gchar     *signal_name,
                            GVariant        *parameters,
                            gpointer         data)

{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_STORE(data));

    if (!eog_list_store_length(EOG_LIST_STORE(data)))
        return;

    EogImagesManagerStorePrivate *priv = EOG_IMAGES_MANAGER_STORE(data)->priv;

    gboolean create_event = FALSE, delete_event = FALSE;

    GVariantIter *iter1, *iter2;

    gchar *class_name;

    gint graph = 0, subject = 0, predicate = 0, object = 0;

    g_variant_get (parameters, "(&sa(iiii)a(iiii))", &class_name, &iter1, &iter2);

    if (g_variant_iter_next(iter1, "(iiii)", &graph, &subject, &predicate, &object))
        delete_event = TRUE;
    if (g_variant_iter_next(iter2, "(iiii)", &graph, &subject, &predicate, &object))
        create_event = TRUE;

    if (create_event) {
        fill_store_cb(data);/*
        if (priv->id_job_manager != 0) {
            g_source_remove(priv->id_job_manager);
            priv->id_job_manager = 0;
        }
        priv->id_job_manager = g_timeout_add(2000, fill_store_cb, data);*/
    }

    if (delete_event) {
        get_updated_list_from_store(EOG_IMAGES_MANAGER_STORE(data));
        eog_images_manager_store_remove_folder(EOG_IMAGES_MANAGER_STORE(data));
    }

    if (priv->sort == SORTED_BY_DATE && !delete_event)
        g_signal_emit(data, signals[SIG_FOLDER_UPDATED], 0);

    g_variant_iter_free (iter1);
    g_variant_iter_free (iter2);
}

void
eog_images_manager_store_set_list_store (EogImagesManagerStore *store, GSList *list)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_STORE(store));

    if (!list)
        return;

    if (store->priv->sort == SORTED_BY_DATE)
        eog_list_store_set_monitoring_with_files(EOG_LIST_STORE (store), FALSE, NULL);
    else
        eog_list_store_set_monitoring_with_files(EOG_LIST_STORE (store), TRUE, NULL);

    if (store->priv->current_store != NULL) {
        g_slist_foreach (store->priv->current_store, (GFunc)g_free, NULL);
        g_slist_free (store->priv->current_store);
        store->priv->current_store = NULL;
    }

    store->priv->current_store = list;
    return;
}