/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#include "eog-utils.h"
#include "eog-jobs-manager.h"
#include "eog-images-manager-nav.h"
#include "eog-images-manager-store.h"

#include <eog-3.0/eog/eog-list-store.h>
#include <libtracker-sparql/tracker-sparql.h>
#include <glib/gi18n.h>

//G_DEFINE_ABSTRACT_TYPE (EogJob, eog_job, G_TYPE_OBJECT);
G_DEFINE_TYPE (EogJobManager,         eog_job_manager,          EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobGetImages,       eog_job_get_images,       EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobThumbnailsList,  eog_job_thumbnails_list,  EOG_TYPE_JOB);



static void     eog_job_manager_class_init     (EogJobManagerClass     *class);
static void     eog_job_manager_init           (EogJobManager          *job);
static void     eog_job_manager_dispose        (GObject              *object);

static void     eog_job_get_images_class_init     (EogJobGetImagesClass     *class);
static void     eog_job_get_images_init           (EogJobGetImages          *job);
static void     eog_job_get_images_dispose        (GObject              *object);

static void     eog_job_thumbnails_list_class_init     (EogJobThumbnailsListClass    *class);
static void     eog_job_thumbnails_list_init           (EogJobThumbnailsList         *job);
static void     eog_job_thumbnails_list_dispose        (GObject                      *object);

static void eog_job_manager_run (EogJob *job);
static void eog_job_get_images_run (EogJob *job);
static void eog_job_thumbnails_list_run (EogJob *job);

static gboolean
notify_finished (EogJob *job)
{
    /* get finished signal */
    guint signal_finished = g_signal_lookup("finished", EOG_TYPE_JOB);

    /* notify job finalization */
    g_signal_emit (job,
                   signal_finished,
                   0);
    return FALSE;
}

/*
 *
 *  JOB MANAGER
 *
 *
 */
static void
get_images_by_folders_tracker (EogJobManager *job_manager, TrackerSparqlCursor *cursor)
{
    while (tracker_sparql_cursor_next(cursor, NULL, NULL)) {

        folderContent *folder = g_new0(folderContent, 1);

        const gchar *path_folder = tracker_sparql_cursor_get_string(cursor, 0, NULL);
        const gchar *folder_name = tracker_sparql_cursor_get_string(cursor, 1, NULL);
        const gchar *icon_folder = tracker_sparql_cursor_get_string(cursor, 2, NULL);

        folder->path_folder = g_strdup(path_folder);
        folder->folder_name = g_strdup(folder_name);
        folder->icon_folder = g_strdup(icon_folder);
        folder->icon_name   = g_strdup(folder_name);

        job_manager->folder = g_slist_prepend(job_manager->folder, folder);
//        eog_images_manager_store_add_folder(EOG_IMAGES_MANAGER_STORE(job_manager->store), path_folder, folder_name, icon_folder, folder_name);
        job_manager->file_list = g_slist_prepend(job_manager->file_list, g_strdup(path_folder));
    }
    return;
}

static void
get_images_by_dates_tracker (EogJobManager *job_manager, TrackerSparqlCursor *cursor)
{
    while (tracker_sparql_cursor_next(cursor, NULL, NULL)) {
        const gchar *icon = tracker_sparql_cursor_get_string(cursor, 0, NULL);
        const gchar *datetime = tracker_sparql_cursor_get_string(cursor, 1, NULL);
        const gchar *yearmonth = tracker_sparql_cursor_get_string(cursor, 2, NULL);

        GTimeVal gTimeVal;
        GDateTime *gDateTime = NULL;
        if (g_time_val_from_iso8601(datetime, &gTimeVal)) {
            gDateTime = g_date_time_new_from_timeval_local(&gTimeVal);
            gchar *month_year = g_date_time_format(gDateTime, "%b %Y");
            month_year[0] = g_ascii_toupper(month_year[0]);

            folderContent *folder = g_new0 (folderContent, 1);
            folder->path_folder = g_strdup(yearmonth);
            folder->folder_name = g_strdup(month_year);
            folder->icon_folder = g_strdup(icon);
            folder->icon_name   = g_strdup(yearmonth);

            //eog_images_manager_store_add_folder(EOG_IMAGES_MANAGER_STORE(job_manager->store), yearmonth, month_year, icon, yearmonth);

            g_free (month_year);
            g_date_time_unref(gDateTime);

            job_manager->folder = g_slist_append(job_manager->folder, folder);
            job_manager->file_list = g_slist_prepend(job_manager->file_list, g_strdup(yearmonth));
        }
    }
    return;
}

static void
eog_job_manager_class_init (EogJobManagerClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_manager_dispose;
    eog_job_class->run      = eog_job_manager_run;
}

static
void eog_job_manager_init (EogJobManager *job)
{
    /* initialize all public and private members to reasonable
       default values. */
    job->sort = SORTED_BY_FOLDERS_NAME;
    job->store     = NULL;
    job->file_list = NULL;
    job->mime_types = NULL;
}

static
void eog_job_manager_dispose (GObject *object)
{
    EogJobManager *job;

    g_return_if_fail (EOG_IS_JOB_MANAGER (object));

    job = EOG_JOB_MANAGER (object);

    /* free all public and private members */
    if (job->store) {
        job->store = NULL;
    }

    if (job->folder) {
        g_slist_foreach (job->folder, (GFunc) g_free, NULL);
        g_slist_free (job->folder);
        job->folder = NULL;
    }

    if (job->current_list)
        g_free (job->current_list);

    if (job->mime_types)
        job->mime_types = NULL;

    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_manager_parent_class)->dispose (object);
}

static void
eog_job_manager_run (EogJob *job)
{
    EogJobManager *job_manager;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_MANAGER (job));

    job_manager     = EOG_JOB_MANAGER (g_object_ref (job));

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* create a list store */
    GError *error = NULL;
    TrackerSparqlConnection *connection = tracker_sparql_connection_get(NULL, &error);
    if (!connection) {
        g_printerr("Couldn't obtain a direct connection to the Tracker store : %s\n",
                   error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    gchar *query = NULL;

    // TODO : Maybe use optional for a better query
    if (job_manager->sort == SORTED_BY_FOLDERS_NAME)
        query = g_strdup_printf("SELECT ?urlfolder ?foldername MIN(?name) WHERE { "
                                "?urn a nfo:Image ; nie:url ?name ; nie:mimeType ?mimetype ; nfo:belongsToContainer ?folder . "
                                "?folder nie:url ?urlfolder ; nfo:fileName ?fname . "
                                //"BIND(CONCAT(UCASE(SUBSTR(?fname, 1, 1)), SUBSTR(?fname, 2)) as ?foldername) . "
                                "BIND(IF (?urlfolder = '%s', '%s', IF(?fname = '%s', '%s', CONCAT(UCASE(SUBSTR(?fname, 1, 1)), SUBSTR(?fname, 2)))) AS ?foldername)"
                                "FILTER (?mimetype IN (%s) && ?urlfolder NOT IN (%s))"
                                "}GROUP BY ?folder ORDER BY ASC(?foldername)",
                                g_filename_to_uri(g_get_home_dir(), NULL, NULL),
                            D_("Various"), "Photos_de_veilles", D_("Screensaver"), job_manager->mime_types, job_manager->current_list != NULL ? job_manager->current_list : "");
    else {
        query = g_strdup_printf("SELECT MIN(?imgUrl) ?date ?ym WHERE{ "
                                "?img a nfo:Image ; nie:url ?imgUrl ; nie:mimeType ?mimetype . "
                                "BIND(tracker:coalesce(nie:contentCreated(?img), nfo:fileLastModified(?img)) AS ?date)"
                                "BIND(SUBSTR(?date, 1, 7) AS ?ym) . "
                                "FILTER (?mimetype IN (%s) && ?ym NOT IN (%s))"
                                "} GROUP BY ?ym ORDER BY ASC(?year)", job_manager->mime_types, job_manager->current_list != NULL ? job_manager->current_list : "");
    }

    TrackerSparqlCursor *cursor = tracker_sparql_connection_query(connection, query, NULL, &error);
    if (error) {
        g_printerr("Couldn't query the Tracker store : %s\n", error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    if (!cursor) {
        g_printerr("Couldn't found result matching the query : %s\n", query);
        return;
    }

    if (!job_manager->store)
        job_manager->store = EOG_LIST_STORE(eog_images_manager_store_new(job_manager->sort));

    if (job_manager->sort == SORTED_BY_FOLDERS_NAME)
        get_images_by_folders_tracker(job_manager, cursor);
    else if (job_manager->sort == SORTED_BY_DATE)
        get_images_by_dates_tracker(job_manager, cursor);

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* job finished */
    job->finished = TRUE;

    g_object_unref (cursor);
    g_object_unref (connection);
    g_free (query);

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
}

/**
 * eog_job_manager_new:
 * @store: (element-type GFile): The current #EogListStore
 *
 * Creates a new #EogJob manager.
 *
 * Returns: A #EogJob.
 */

EogJob *
eog_job_manager_new (guint sort, EogListStore *store, gchar *current_list, gchar *mime_types)
{
//    g_return_val_if_fail(EOG_IS_LIST_STORE(store), NULL);

    EogJobManager *job;

    job = g_object_new (EOG_TYPE_JOB_MANAGER, NULL);
    job->sort = sort;
    job->store = store;
    job->current_list = g_strdup(current_list);
    job->file_list = NULL;
    job->mime_types = mime_types;

    return EOG_JOB (job);
}

/*
 *
 * JOB IMAGES
 *
 */

static void
eog_job_get_images_class_init (EogJobGetImagesClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_get_images_dispose;
    eog_job_class->run      = eog_job_get_images_run;
}

static
void eog_job_get_images_init (EogJobGetImages *job)
{
    /* initialize all public and private members to reasonable
       default values. */
    job->file_list = NULL;
}

static
void eog_job_get_images_dispose (GObject *object)
{
    EogJobGetImages *job;

    g_return_if_fail (EOG_IS_JOB_GET_IMAGES (object));

    job = EOG_JOB_GET_IMAGES (object);

    /* free all public and private members */

    if (job->file_list) {
//        g_slist_foreach (job->file_list, (GFunc) g_free, NULL);
//        g_slist_free (job->file_list);
        job->file_list = NULL;
    }

    if (job->current_list)
        g_free (job->current_list);

    g_free (job->key);
    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_get_images_parent_class)->dispose (object);
}

static void
eog_job_get_images_run (EogJob *job)
{
    EogJobGetImages *job_get_images;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_GET_IMAGES (job));

    job_get_images     = EOG_JOB_GET_IMAGES (g_object_ref (job));

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* create a list store */
    GError *error = NULL;
    TrackerSparqlConnection *connection = tracker_sparql_connection_get(NULL, &error);
    if (!connection) {
        g_printerr("Couldn't obtain a direct connection to the Tracker store : %s\n",
                   error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    gchar *current_list = job_get_images->current_list ? job_get_images->current_list : "";
    gchar *query = g_strdup_printf("SELECT ?pathImage nfo:fileName(?images) nfo:height(?images) nfo:width(?images) ?mimetype ?date nfo:fileSize(?images) WHERE{ "
                                   "?images a nfo:Image ; nie:mimeType ?mimetype ; nie:url ?pathImage . "
                                   "BIND(tracker:coalesce(nie:contentCreated(?images), nfo:fileLastModified(?images)) AS ?date) . "
                                   "FILTER(CONTAINS(?date, '%s') && ?mimetype IN (%s) && ?pathImage NOT IN (%s)) ."
                                   "} ORDER BY ASC(nfo:fileName(?images))", job_get_images->key, job_get_images->mime_types, current_list);

    TrackerSparqlCursor *cursor = tracker_sparql_connection_query(connection, query, NULL, &error);
    if (error) {
        g_printerr("Couldn't query the Tracker store : %s\n", error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    if (!cursor)
        g_printerr("Couldn't found result matching the query : %s\n", query);

    while (tracker_sparql_cursor_next(cursor, NULL, &error)) {
        const gchar *path = tracker_sparql_cursor_get_string(cursor, 0, NULL);
        const gchar *name = tracker_sparql_cursor_get_string(cursor, 1, NULL);

        GFile *file_path = g_file_new_for_uri(path);

        job_get_images->file_list = g_slist_prepend(job_get_images->file_list, file_path);

        if (job_get_images->update_store && job_get_images->store != NULL && !eog_images_manager_nav_is_image_in_gallery(GTK_TREE_MODEL(job_get_images->store), file_path, NULL)) {
            EogImage *img = eog_image_new_file(file_path, name);
            if (img != NULL) {
                eog_list_store_append_image(job_get_images->store, img);
		eog_image_autorotate (img);
	    }
        }
    }

    g_object_unref(connection);
    g_object_unref(cursor);
    g_free (query);
//    g_free (current_list);

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
}

/**
 * eog_job_get_images_new:
 * @store: (element-type GFile): The current #EogListStore
 *
 * Creates a new #EogJob get_images.
 *
 * Returns: A #EogJob.
 */

EogJob *
eog_job_get_images_new (gchar *mime_types, gchar *key, gboolean update_store, EogListStore *store, gchar *current_list)
{
    g_return_val_if_fail(mime_types != NULL, NULL);
    g_return_val_if_fail(key != NULL, NULL);

    EogJobGetImages *job;

    job = g_object_new (EOG_TYPE_JOB_GET_IMAGES, NULL);
    job->mime_types = mime_types;
    job->key = g_strdup(key);
    job->update_store = update_store;
    job->store = store;
    job->file_list = NULL;
    job->current_list = current_list;

    return EOG_JOB (job);
}


// THUMBNAILS LIST
static void
eog_job_thumbnails_list_class_init (EogJobThumbnailsListClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_thumbnails_list_dispose;
    eog_job_class->run      = eog_job_thumbnails_list_run;
}

static void
eog_job_thumbnails_list_init (EogJobThumbnailsList *job)
{
    /* initialize all public and private members to reasonable
       default values. */
    job->file_list = NULL;
    job->list_thumbnails = NULL;
}

static
void eog_job_thumbnails_list_dispose (GObject *object)
{
    EogJobThumbnailsList *job;

    g_return_if_fail (EOG_IS_JOB_THUMBNAILS_LIST(object));

    job = EOG_JOB_THUMBNAILS_LIST(object);

    /* free all public and private members */

    if (job->file_list) {
//        g_slist_foreach (job->file_list, (GFunc) g_object_unref, NULL);
//        g_slist_free (job->file_list);
        job->file_list = NULL;
    }

    if (job->list_thumbnails) {
//        g_slist_foreach (job->list_thumbnails, (GFunc) g_object_unref, NULL);
//        g_slist_free (job->list_thumbnails);
        job->list_thumbnails = NULL;
    }

    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_thumbnails_list_parent_class)->dispose (object);
}
#include <eog-3.0/eog/eog-thumbnail.h>
static void
eog_job_thumbnails_list_run (EogJob *job)
{
    EogJobThumbnailsList *thumbnails;

    /* clean previous errors */
    if (job->error) {
        g_error_free (job->error);
        job->error = NULL;
    }
    /* initialization */
    g_return_if_fail (EOG_IS_JOB_THUMBNAILS_LIST(job));

    thumbnails = EOG_JOB_THUMBNAILS_LIST(g_object_ref (job));

    gchar     *original_width;
    gchar     *original_height;
    gint       width;
    gint       height;

    /* --- leave critical section --- */
    g_mutex_lock (job->mutex);
    GSList *it;
    it = thumbnails->file_list;
    if(!it)
        return;
    GtkTreeIter iter;
    gint pos = eog_list_store_get_pos_by_image(thumbnails->store, EOG_IMAGE (it->data));
    GtkTreePath *path = gtk_tree_path_new_from_indices(pos, -1);

    gtk_tree_model_get_iter(GTK_TREE_MODEL (thumbnails->store), &iter, path);

    while (gtk_tree_model_iter_next (GTK_TREE_MODEL(thumbnails->store), &iter) && it != NULL) {
        /* try to load the image thumbnail from cache */
        GdkPixbuf *thumbnail = eog_thumbnail_load (EOG_IMAGE (it->data),
                                                   &job->error);
        if (thumbnail) {
            /* create the image thumbnail */
            original_width  = g_strdup (gdk_pixbuf_get_option
                                                (thumbnail,
                                                 "tEXt::Thumb::Image::Width"));
            original_height = g_strdup (gdk_pixbuf_get_option
                                                (thumbnail,
                                                 "tEXt::Thumb::Image::Height"));

            thumbnail = eog_thumbnail_fit_to_size (thumbnail,
                                                90);

            if (original_width) {
                sscanf (original_width, "%i", &width);
                g_object_set_data (G_OBJECT (thumbnail),
                                   EOG_THUMBNAIL_ORIGINAL_WIDTH,
                                   GINT_TO_POINTER (width));
                g_free (original_width);
            }

            if (original_height) {
                sscanf (original_height, "%i", &height);
                g_object_set_data (G_OBJECT (thumbnail),
                                   EOG_THUMBNAIL_ORIGINAL_HEIGHT,
                                   GINT_TO_POINTER (height));
                g_free (original_height);
            }
            thumbnails->list_thumbnails = g_slist_prepend(thumbnails->list_thumbnails, thumbnail);
           pos = eog_list_store_get_pos_by_image(thumbnails->store, EOG_IMAGE (it->data));

            gtk_list_store_set (GTK_LIST_STORE (thumbnails->store), &iter,
                                EOG_LIST_STORE_EOG_JOB, NULL,
                                EOG_LIST_STORE_THUMB_SET, TRUE,
                                EOG_LIST_STORE_THUMBNAIL, thumbnail,
                                -1);
        }
        it = it->next;
    }

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
}

/**
 * eog_job_get_images_new:
 * @store: (element-type GFile): The current #EogListStore
 *
 * Creates a new #EogJob thumbnails_list.
 *
 * Returns: A #EogJob.
 */

EogJob *
eog_job_thumbnails_list_new (EogListStore *store, GSList *images)
{

    EogJobThumbnailsList *job;

    job = g_object_new (EOG_TYPE_JOB_THUMBNAILS_LIST, NULL);
    job->store = store;
    job->file_list = images;

    return EOG_JOB (job);
}
