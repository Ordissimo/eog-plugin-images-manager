/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_JOB_MANAGER_H
#define EOG_JOB_MANAGER_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-jobs.h>
#include "eog-images-manager-store.h"

G_BEGIN_DECLS

#define EOG_TYPE_JOB_MANAGER                (eog_job_manager_get_type ())
#define EOG_JOB_MANAGER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_MANAGER, EogJobManager))
#define EOG_JOB_MANAGER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_MANAGER, EogJobManagerClass))
#define EOG_IS_JOB_MANAGER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_MANAGER))
#define EOG_IS_JOB_MANAGER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_MANAGER))
#define EOG_JOB_MANAGER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_MANAGER, EogJobManagerClass))

#define EOG_TYPE_JOB_GET_IMAGES                (eog_job_get_images_get_type ())
#define EOG_JOB_GET_IMAGES(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_GET_IMAGES, EogJobGetImages))
#define EOG_JOB_GET_IMAGES_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_GET_IMAGES, EogJobGetImagesClass))
#define EOG_IS_JOB_GET_IMAGES(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_GET_IMAGES))
#define EOG_IS_JOB_GET_IMAGES_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_GET_IMAGES))
#define EOG_JOB_GET_IMAGES_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_GET_IMAGES, EogJobGetImagesClass))

#define EOG_TYPE_JOB_THUMBNAILS_LIST                (eog_job_thumbnails_list_get_type ())
#define EOG_JOB_THUMBNAILS_LIST(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_THUMBNAILS_LIST, EogJobThumbnailsList))
#define EOG_JOB_THUMBNAILS_LIST_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_THUMBNAILS_LIST, EogJobThumbnailsListClass))
#define EOG_IS_JOB_THUMBNAILS_LIST(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_THUMBNAILS_LIST))
#define EOG_IS_JOB_THUMBNAILS_LIST_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_THUMBNAILS_LIST))
#define EOG_JOB_THUMBNAILS_LIST_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_THUMBNAILS_LIST, EogJobThumbnailsListClass))

typedef struct _EogJobManager          EogJobManager;
typedef struct _EogJobManagerClass     EogJobManagerClass;

typedef struct _EogJobGetImages          EogJobGetImages;
typedef struct _EogJobGetImagesClass     EogJobGetImagesClass;

typedef struct _EogJobThumbnailsList          EogJobThumbnailsList;
typedef struct _EogJobThumbnailsListClass     EogJobThumbnailsListClass;

struct _EogJobManager
{
    EogJob           parent;

    guint            sort;
    EogListStore    *store;
    gchar           *current_list;
    GSList          *file_list;
    gchar           *mime_types;

    GSList          *folder;
};

struct _EogJobManagerClass
{
    EogJobClass      parent_class;
};

struct _EogJobGetImages
{
    EogJob           parent;

    GSList          *file_list;
    gchar           *current_list;
    EogListStore    *store;
    gchar           *mime_types;
    gchar           *key;

    gboolean         update_store;
};

struct _EogJobGetImagesClass
{
    EogJobClass      parent_class;
};

struct _EogJobThumbnailsList
{
    EogJob           parent;

    EogListStore    *store;
    GSList          *file_list;
    GSList          *list_thumbnails;
};

struct _EogJobThumbnailsListClass
{
    EogJobClass parent_class;
};

GType 	 eog_job_manager_get_type     (void) G_GNUC_CONST;
EogJob 	*eog_job_manager_new          ( guint            sort,
                                        EogListStore    *store,
                                        gchar           *current_list,
                                        gchar           *mime_types);

GType 	 eog_job_get_images_get_type     (void) G_GNUC_CONST;
EogJob 	*eog_job_get_images_new          (gchar           *mime_types,
                                          gchar           *key,
                                          gboolean         from_signal,
                                          EogListStore    *store,
                                          gchar           *current_list);

GType 	 eog_job_thumbnails_list_get_type     (void) G_GNUC_CONST;
EogJob 	*eog_job_thumbnails_list_new          (EogListStore *store, GSList *images);

G_END_DECLS

#endif //EOG_JOB_MANAGER_H
