/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_UTILS_H
#define EOG_UTILS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-window.h>
#include <eog-3.0/eog/eog-thumb-view.h>
#include <eog-3.0/eog/eog-thumb-nav.h>

#define D_(string) dgettext (EIM_PACKAGE, string)

G_BEGIN_DECLS


G_GNUC_INTERNAL
GtkWidget *eog_utils_get_widget_by_name (GtkWidget *parent,
                                         const gchar *name);

G_GNUC_INTERNAL
void eog_utils_set_mode_multiple_rows (EogWindow *window);

G_GNUC_INTERNAL
void eog_utils_set_mode_single_row (EogWindow *window);

void eog_utils_resize_thumbnails (EogThumbNav *nav, EogThumbView *thumbview, guint new_size);
G_END_DECLS

#endif //EOG_UTILS_H
