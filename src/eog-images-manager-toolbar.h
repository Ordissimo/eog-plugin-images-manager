/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_IMAGES_MANAGER_TOOLBAR_H
#define EOG_IMAGES_MANAGER_TOOLBAR_H

#include <eog-3.0/eog/eog-thumb-nav.h>
#include <eog-3.0/eog/eog-window.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_IMAGES_MANAGER_TOOLBAR		(eog_images_manager_toolbar_get_type ())
#define EOG_IMAGES_MANAGER_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_IMAGES_MANAGER_TOOLBAR, EogImagesManagerToolbar))
#define EOG_IMAGES_MANAGER_TOOLBAR_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_IMAGES_MANAGER_TOOLBAR, EogImagesManagerToolbarClass))
#define EOG_IS_IMAGES_MANAGER_TOOLBAR(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_IMAGES_MANAGER_TOOLBAR))
#define EOG_IS_IMAGES_MANAGER_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_IMAGES_MANAGER_TOOLBAR))
#define EOG_IMAGES_MANAGER_TOOLBAR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_IMAGES_MANAGER_TOOLBAR, EogImagesManagerToolbarClass))

typedef struct _EogImagesManagerToolbar EogImagesManagerToolbar;
typedef struct _EogImagesManagerToolbarClass _EogImagesManagerToolbarClass;
typedef struct _EogImagesManagerToolbarPrivate EogImagesManagerToolbarPrivate;

struct _EogImagesManagerToolbar
{
    GtkToolbar parent_instance;

    EogImagesManagerToolbarPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogImagesManagerToolbarClass	EogImagesManagerToolbarClass;

struct _EogImagesManagerToolbarClass
{
    GtkToolbarClass parent_class;
    void (* process_started) (EogImagesManagerToolbar *toolbar);
    void (* process_stopped) (EogImagesManagerToolbar *toolbar);
};

/*
 * Public methods
 */
GType	eog_images_manager_toolbar_get_type		(void) G_GNUC_CONST;

GtkWidget *eog_images_manager_toolbar_new (EogThumbNav *nav, EogWindow *window, GtkStack *stack);

void eog_images_manager_toolbar_hide_items(EogImagesManagerToolbar *toolbar, gboolean writable, gboolean hide);

G_END_DECLS

#endif //EOG_IMAGES_MANAGER_TOOLBAR_H
