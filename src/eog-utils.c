/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#include "eog-utils.h"
#include <glib.h>
#include <string.h>

GtkWidget *
eog_utils_get_widget_by_name (GtkWidget *parent, const gchar *name)
{
    g_return_val_if_fail (GTK_IS_WIDGET (parent), NULL);
    g_return_val_if_fail (name != NULL, NULL);

    if (!g_strcmp0(gtk_widget_get_name(GTK_WIDGET(parent)), name))
        return parent;
    else if (GTK_IS_CONTAINER(parent)){
        GList *children = gtk_container_get_children(GTK_CONTAINER(parent));
        GList *it;
        for(it = children; it != NULL; it = it->next) {
            GtkWidget *child = eog_utils_get_widget_by_name(GTK_WIDGET(it->data), name);
            if (child != NULL) {
                g_list_free(children);
                return child;
            }
        }
    } else if (GTK_IS_BIN(parent)){
        GtkWidget *child = gtk_bin_get_child(GTK_BIN(parent));
        return eog_utils_get_widget_by_name(child, name);
    }
    return NULL;
}

void
eog_utils_set_mode_multiple_rows (EogWindow *window)
{
    g_return_if_fail (EOG_IS_WINDOW (window));

    GtkWidget *eog_nav = eog_window_get_thumb_nav(window);
//    EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV (eog_nav));

//    if (mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS)
//        return;

    GtkWidget *sidebar = eog_window_get_sidebar (window);
    GtkWidget *hpaned = gtk_widget_get_parent (sidebar);
    GtkWidget *box_paned = gtk_widget_get_parent(hpaned);
    GtkWidget *eog_view = eog_window_get_thumb_view(window);
    GtkWidget *scrolled_view = gtk_widget_get_parent(eog_view);

    GList *renderers = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (eog_view));
    GtkCellRenderer *text_cell = renderers->next->data;
    g_object_set (text_cell, "visible", FALSE, NULL);
    g_list_free (renderers);

    gtk_widget_set_visible (hpaned, FALSE);
    gtk_box_set_child_packing(GTK_BOX(box_paned), GTK_WIDGET(eog_nav), TRUE, TRUE, 0, GTK_PACK_START);

    gtk_widget_set_vexpand (eog_view, TRUE);
//    eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);

    gtk_scrolled_window_set_propagate_natural_height (GTK_SCROLLED_WINDOW(scrolled_view), TRUE);

    gtk_icon_view_set_selection_mode (GTK_ICON_VIEW (eog_view),GTK_SELECTION_SINGLE);

    GtkAllocation alloc_nav;
    gtk_widget_get_allocation (GTK_WIDGET(eog_nav), &alloc_nav);

    gtk_icon_view_unselect_all(GTK_ICON_VIEW (eog_view));
    return;
}

void
eog_utils_set_mode_single_row (EogWindow *window)
{
    g_return_if_fail (EOG_IS_WINDOW (window));

    GtkWidget *eog_nav = eog_window_get_thumb_nav(window);
//    EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV (eog_nav));

//    if (mode == EOG_THUMB_NAV_MODE_ONE_ROW)
//        return;

    GtkWidget *sidebar = eog_window_get_sidebar (window);
    GtkWidget *hpaned = gtk_widget_get_parent (sidebar);
    GtkWidget *box_paned = gtk_widget_get_parent(hpaned);
    GtkWidget *eog_view = eog_window_get_thumb_view(window);

    GList *renderers = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (eog_view));
    GtkCellRenderer *text_cell = renderers->next->data;
    g_object_set (text_cell, "visible", TRUE, NULL);
    g_list_free (renderers);

//    eog_thumb_nav_set_mode(EOG_THUMB_NAV(eog_nav), EOG_THUMB_NAV_MODE_ONE_ROW);

    gtk_icon_view_set_selection_mode (GTK_ICON_VIEW (eog_view), GTK_SELECTION_BROWSE);
    gtk_icon_view_set_item_padding (GTK_ICON_VIEW(eog_view), 0);
    gtk_icon_view_set_margin (GTK_ICON_VIEW (eog_view), 0);

    gtk_box_set_child_packing(GTK_BOX(box_paned), GTK_WIDGET(eog_nav), FALSE, FALSE, 0, GTK_PACK_START);

    gtk_widget_set_vexpand (eog_view, FALSE);
    gtk_widget_show_all (hpaned);

    // If plugin-toolbar is active
    GtkWidget *eog_scrollview = eog_window_get_view (window);
    GtkWidget *parent_scrollview = gtk_widget_get_parent(eog_scrollview);
    GList *children = gtk_container_get_children(GTK_CONTAINER (parent_scrollview));
    GtkWidget *area_progress = g_list_nth_data (children, 1);
    if (GTK_IS_DRAWING_AREA(area_progress))
        gtk_widget_hide (area_progress);
    g_list_free (children);

    return;
}

static void
resize_cells (gpointer data, gpointer user_data)
{
    if (GTK_IS_CELL_RENDERER_PIXBUF(data)) {
        guint size = GPOINTER_TO_INT(user_data);

        g_object_set (GTK_CELL_RENDERER(data),
                      "height", size,
                      "width", size,
                      "yalign", 0.5,
                      "xalign", 0.5,
                      NULL);
//        gtk_cell_renderer_set_fixed_size(data, size, size);
    }
    return;
}

void
eog_utils_resize_thumbnails (EogThumbNav *nav, EogThumbView *thumbview, guint new_size)
{
    EogListStore     *store = EOG_LIST_STORE(gtk_icon_view_get_model (GTK_ICON_VIEW (thumbview)));
    g_object_set (store, "size-thumbnail", new_size, NULL);

    gint size_thumb;
    if (eog_thumb_nav_get_mode (nav) == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
        gint width_thumbview = gtk_widget_get_allocated_width(GTK_WIDGET(thumbview));

        gint val_thumbnails = new_size;
        gint val_col = width_thumbview / val_thumbnails;
        gtk_icon_view_set_columns(GTK_ICON_VIEW(thumbview), val_col);

        // TODO Check why 33
        width_thumbview -= 33; //90;//gtk_widget_get_margin_end(GTK_WIDGET (thumbview));
        size_thumb = width_thumbview / val_col;
    } else {
        gint len = eog_list_store_length (store);
        gtk_icon_view_set_columns(GTK_ICON_VIEW(thumbview), len);
        size_thumb = new_size;
    }

    GList *renderer = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (thumbview));
    g_list_foreach(renderer, resize_cells, GINT_TO_POINTER(size_thumb));

    GtkTreePath *path1 = gtk_tree_path_new_first ();
    gint n_items = gtk_tree_model_iter_n_children(GTK_TREE_MODEL (store), NULL);
    GtkTreePath *path2 = gtk_tree_path_new_from_indices(n_items - 1, -1);

    GtkTreeIter iter;
    gint thumb = 0;
    gint end_thumb = gtk_tree_path_get_indices(path2)[0];
    gboolean result;
    gboolean thumb_set = FALSE;
    for (result = gtk_tree_model_get_iter(GTK_TREE_MODEL (store), &iter, path1);
         result && thumb <= end_thumb + 1;
         result = gtk_tree_model_iter_next(GTK_TREE_MODEL (store), &iter), thumb++) {
        gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, EOG_LIST_STORE_THUMB_SET, &thumb_set, -1);
        if (thumb_set)
            eog_list_store_thumbnail_refresh(store, &iter);
    }

    gtk_tree_path_free(path1);
    gtk_tree_path_free(path2);

    g_object_set(thumbview,
                 "activate-on-single-click", TRUE, "margin", 10, "item-padding", 1, NULL);
}
