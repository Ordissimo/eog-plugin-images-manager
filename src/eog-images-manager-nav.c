/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

/*!
 * \file eog-images-manager-nav.c
 * \brief GtkBox to navigate in folders.
 *
 * Display folders and choose the display.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "eog-utils.h"
#include "eog-jobs-manager.h"
#include "eog-images-manager-nav.h"
#include "eog-images-manager-store.h"
#include "eog-images-manager-toolbar.h"
#include "eog-images-manager-scheduler.h"

#include <glib/gi18n.h>
#include <eog-3.0/eog/eog-window.h>
#include <eog-3.0/eog/eog-thumb-nav.h>
#include <eog-3.0/eog/eog-list-store.h>
#include <eog-3.0/eog/eog-scroll-view.h>

#define EOG_CONF_UI		"org.gnome.eog.ui"
#define GALLERY_SIZE    "image-gallery-thumb-size"

struct _EogImagesManagerNavPrivate{
    EogWindow *window;
    GtkWidget *stack_toolbars;/*!< GtkStack containing the toolbars. */
    GtkWidget *main_toolbar;/*!< GtkToolbar containing the main-toolbar. */
    GtkListStore *store_manager; /*!< EogListStore containing folders. */
    GtkWidget *eog_thumbnav;
    GtkWidget *view_manager; /*!< GtkIconView containing the liststore. */
    GtkWidget *buttonbox;
    GtkWidget *folder_info;

    gboolean from_eog;
    GtkTreePath *folder_selected; /*!< GtkTreePath containing the the folder selected before quit eog. */
    gchar *name_folder; /*!< String containing the previous name before quit eog. */

    gchar *label_album;
    gchar *label_folder;
    gchar *len_images;
    gchar *home_folder;
    gchar *screensaver;

    guint signal_selection; /*!< ID of signal "selection-changed" from GtkIconView. */
    guint signal_updated;

    guint image_removed;
    guint image_added;

    guint sort;

    gboolean store_empty;

    EogJob *job_manager;
    GString *mimetypes;

    /* members for eog store*/
    GHashTable  *monitors_dates;

    GSettings *gallery_settings;
    gboolean display_gallery;
    guint id_load_gallery;

    EogJob *id_job_dates;
};

enum {
    PROP_0,
    PROP_SORT,
    PROP_EOG,
    PROP_THUMBVIEW,
    PROP_WINDOW,
    PROP_STACK,
    PROP_ITEM_SELECTED,
};

enum {
    SIGNAL_IMAGES_LOADING,
    SIGNAL_LAST
};

static gint signals[SIGNAL_LAST];

G_DEFINE_TYPE_WITH_PRIVATE (EogImagesManagerNav, eog_images_manager_nav, GTK_TYPE_BOX)//EOG_TYPE_THUMB_NAV)

static void eog_images_manager_nav_update_gallery (EogImagesManagerNav *nav);
static gboolean eog_images_manager_nav_get_images (/* EogImagesManagerNav * */gpointer nav);

static void dates_monitors_gallery_cb (GFileMonitor *monitor,
                                       GFile *file,
                                       GFile *other_file,
                                       GFileMonitorEvent event,
                                       EogImagesManagerNav *nav);

static void
update_range_thumbnail(EogImagesManagerNav *nav)
{
    EogImagesManagerNavPrivate *priv = nav->priv;
    GtkTreePath *path_start, *path_end;
    if(gtk_icon_view_get_visible_range(GTK_ICON_VIEW(priv->view_manager), &path_start, &path_end)) {
        guint path = gtk_tree_path_get_indices(path_start)[0];
        guint end_path = gtk_tree_path_get_indices(path_end)[0];
        gboolean result;
        GtkTreeIter iter;
        for(result = gtk_tree_model_get_iter(GTK_TREE_MODEL(priv->store_manager), &iter, path_start);
            result && path <= end_path;
            result = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store_manager), &iter), path++)
            eog_list_store_thumbnail_refresh(EOG_LIST_STORE(priv->store_manager), &iter);

        if (path_start)
            gtk_tree_path_free (path_start);
        if (path_end)
            gtk_tree_path_free (path_end);
    } else if (eog_list_store_length(EOG_LIST_STORE(priv->store_manager)) == 1){
        GtkTreeIter iter;
        path_start = gtk_tree_path_new_first();
        gboolean res = gtk_tree_model_get_iter(GTK_TREE_MODEL(priv->store_manager), &iter, path_start);
        if (res)
            eog_list_store_thumbnail_refresh(EOG_LIST_STORE(priv->store_manager), &iter);

        if (path_start)
            gtk_tree_path_free (path_start);
    }

    return;
}

static void
select_folder_by_name  (EogImagesManagerNav *nav)
{
  EogImagesManagerNavPrivate *priv = nav->priv;

  if (priv->name_folder != NULL){
    GtkTreeIter iter;
    gboolean found = FALSE;
    gchar *folder_key = NULL;
    gboolean valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store_manager), &iter);
    while (valid) {
      gtk_tree_model_get(GTK_TREE_MODEL(priv->store_manager), &iter, FOLDER_KEY, &folder_key, -1);
      found = (strcmp(priv->name_folder, folder_key) == 0) ? TRUE : FALSE;
      if (found) {
	priv->folder_selected = gtk_tree_model_get_path(GTK_TREE_MODEL(priv->store_manager), &iter);
	break;
      }
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store_manager), &iter);
    }
    g_free (folder_key);
    if (!found)
      priv->folder_selected = gtk_tree_path_new_first();
    g_free (priv->name_folder);
    priv->name_folder = NULL;
  } else {
    priv->folder_selected = gtk_tree_path_new_first();
  }

  gtk_icon_view_scroll_to_path(GTK_ICON_VIEW(priv->view_manager), priv->folder_selected, TRUE, 0, 1);
  gtk_icon_view_select_path (GTK_ICON_VIEW(priv->view_manager), priv->folder_selected);

  return;
}

/**
 * \fn store_loaded_cb
 * @widget: a #GtkWidget
 * @data: a #gpointer
 *
 * Signal connected to "folders-loaded".
 * When we finished to load folders in the the liststore we select one.
 *
 */
static void
store_loaded_cb (GtkListStore *store, gpointer data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(data));
    EogImagesManagerNav *nav = EOG_IMAGES_MANAGER_NAV (data);
    EogImagesManagerNavPrivate *priv = nav->priv;

    eog_thumb_view_set_model(EOG_THUMB_VIEW(priv->view_manager), EOG_LIST_STORE(priv->store_manager));

    if (!eog_list_store_length(EOG_LIST_STORE (priv->store_manager))) {
        priv->store_empty = TRUE;
        return;
    }

    if(eog_thumb_view_get_n_selected(EOG_THUMB_VIEW(priv->view_manager)) != 0) {
        /*
         * No choice since eog sometimes doesn't load thumbnail added in the visible range
         */
        update_range_thumbnail(nav);
        return;
    }
    update_range_thumbnail(nav);

    select_folder_by_name (nav);
    if (!priv->from_eog)
      gtk_icon_view_item_activated(GTK_ICON_VIEW(priv->view_manager), priv->folder_selected);
    else {
      //      g_signal_emit(nav, signals[SIGNAL_IMAGES_LOADING], 0, TRUE);
      priv->display_gallery = TRUE;
    }
    return;
}

/**
 * \fn select_folder_cb
 * @thumbview: an #EogThumbView
 * @nav: an #EogImagesManagerNav
 *
 * Signal connected to "selection-changed".
 * We want always a selection. So if it disappear because of a change, we select the previous folder.
 *
 */
static void
select_folder_cb (EogThumbView *thumbview, EogImagesManagerNav *nav)
{
    EogImagesManagerNavPrivate *priv = nav->priv;
    if (eog_thumb_view_get_first_selected_image (thumbview) != NULL)
        return;

    if (priv->folder_selected == NULL)
        priv->folder_selected = gtk_tree_path_new_first();
    else if (!gtk_tree_path_prev(priv->folder_selected)) {
        GtkTreeModel *model_manager = gtk_icon_view_get_model(GTK_ICON_VIEW (thumbview));
        gint len_folders = eog_list_store_length(EOG_LIST_STORE (model_manager));
        if (!len_folders) {
            gtk_widget_hide(priv->folder_info);
            return;
        }
    }

    gtk_icon_view_scroll_to_path(GTK_ICON_VIEW(priv->view_manager), priv->folder_selected, FALSE, 0, 0);
    gtk_icon_view_select_path(GTK_ICON_VIEW(priv->view_manager), priv->folder_selected);
    gtk_icon_view_item_activated(GTK_ICON_VIEW(priv->view_manager), priv->folder_selected);
    return;
}

/**
 *
 * @widget: A #GtkWidget
 * @image: an #EogImage
 * @data: a #gpointer
 *
 * Signal connected to "folder-updated".
 * This signal emit when an update occurs in the manager.
 *
 */
static void
store_updated_cb (GtkWidget *widget, EogImagesManagerNav *nav)
{
    EogImagesManagerNavPrivate *priv = nav->priv;

    if (priv->sort == SORTED_BY_DATE) {
        eog_images_manager_nav_update_gallery (nav);
    } else if (priv->store_empty) {
        g_message ("Store empty ?");
        select_folder_cb (EOG_THUMB_VIEW (priv->view_manager), nav);
        priv->store_empty = FALSE;
    }
    update_range_thumbnail(nav);
    return;
}

static gboolean
resize_thumbnails_idle (gpointer data)
{
    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(data)->priv;

    GtkWidget *eog_nav = eog_window_get_thumb_nav (priv->window);
    GtkWidget *eog_thumbview = eog_window_get_thumb_view (priv->window);

    eog_utils_resize_thumbnails(EOG_THUMB_NAV (eog_nav), EOG_THUMB_VIEW (eog_thumbview), g_settings_get_enum (priv->gallery_settings, GALLERY_SIZE));

    return G_SOURCE_REMOVE;
}

static void test_unset (EogImagesManagerNav     *nav)
{
  EogImagesManagerNavPrivate *priv = nav->priv;
  GtkTreePath *path1, *path2;
  GtkWidget *eog_thumbview = eog_window_get_thumb_view (priv->window);

  if (!gtk_icon_view_get_visible_range (GTK_ICON_VIEW (eog_thumbview), &path1, &path2))
    return;

  EogListStore *store = eog_window_get_store (priv->window);
  gint start =  gtk_tree_path_get_indices (path1) [0];
  gint end_thumb =  gtk_tree_path_get_indices (path2) [0];

  GtkTreeIter iter;
  gint thumb = start;
  gboolean result;
  GtkTreePath *path = gtk_tree_path_new_from_indices (start, -1);

  for (result = gtk_tree_model_get_iter (GTK_TREE_MODEL (store), &iter, path);
	     result && thumb < end_thumb;
	     result = gtk_tree_model_iter_next (GTK_TREE_MODEL (store), &iter), thumb++) {
    eog_list_store_thumbnail_unset (store, &iter);
  }
  
  return;
}

static void
activate_folder_cb (GtkIconView *iconview,
                    GtkTreePath *path,
                    EogImagesManagerNav     *nav)
{
    EogImagesManagerNavPrivate *priv = nav->priv;

    if (priv->folder_selected)
        gtk_tree_path_free (priv->folder_selected);
    priv->folder_selected = gtk_tree_path_copy(path);

    gchar *folder_key = NULL;
    EogImage *image = NULL;
    GtkTreeIter iter;
    gtk_tree_model_get_iter(GTK_TREE_MODEL(priv->store_manager), &iter, path);
    gtk_tree_model_get(GTK_TREE_MODEL(priv->store_manager), &iter, EOG_LIST_STORE_EOG_IMAGE, &image, FOLDER_KEY, &folder_key, -1);

    GtkWidget *eog_nav = eog_window_get_thumb_nav(priv->window);
    EogThumbNavMode eog_mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV (eog_nav));

    if (eog_mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
        gboolean set_items = (priv->sort == SORTED_BY_FOLDERS_NAME) ? TRUE : FALSE;
        eog_images_manager_toolbar_hide_items(EOG_IMAGES_MANAGER_TOOLBAR(priv->main_toolbar), TRUE, set_items);
    }

    if (priv->name_folder && strcmp (priv->name_folder, folder_key) == 0) {
        EogListStore *eog_store = eog_window_get_store (priv->window);
        if (eog_mode == EOG_THUMB_NAV_MODE_ONE_ROW && eog_list_store_length(eog_store) > 1) {
            eog_thumb_nav_set_mode(EOG_THUMB_NAV(eog_nav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);
            gint size_thumbnail = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
            g_object_set (eog_window_get_store (priv->window), "size-thumbnail", size_thumbnail, NULL);
            g_idle_add (resize_thumbnails_idle, nav);
            //eog_utils_resize_thumbnails (EOG_THUMB_NAV(eog_nav), EOG_THUMB_VIEW(eog_window_get_thumb_view(priv->window)), size_thumbnail);
            gtk_stack_set_visible_child_name(GTK_STACK(priv->stack_toolbars), "toolbar-manager");
        }
        return;
    }

    priv->name_folder = g_strdup(folder_key);

    g_signal_emit(nav, signals[SIGNAL_IMAGES_LOADING], 0, TRUE);
    test_unset (nav);
    g_idle_add (eog_images_manager_nav_get_images, nav);
    //gtk_icon_view_set_model (GTK_ICON_VIEW (eog_window_get_thumb_view (priv->window)), NULL);
    //    eog_images_manager_nav_get_images(nav);

    g_free (folder_key);
    g_object_unref (image);
}

gboolean
configure_nav_cb (gpointer data)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_NAV(data), G_SOURCE_REMOVE);

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(data)->priv;
    GtkIconView *view_manager = GTK_ICON_VIEW(priv->view_manager);
    gtk_icon_view_set_selection_mode (view_manager,  GTK_SELECTION_BROWSE);
    gtk_icon_view_set_activate_on_single_click(view_manager, TRUE);
    gtk_icon_view_set_columns(view_manager, 2);
    gtk_widget_set_has_tooltip(GTK_WIDGET(view_manager), FALSE);

    gtk_widget_set_margin_end (GTK_WIDGET(priv->view_manager), 15);
    /* gtk_widget_set_margin_end (eog_thumbview, 30); */
    /* gtk_widget_set_margin_bottom (eog_thumbview, 15); */

    GtkWidget *eog_nav = eog_window_get_thumb_nav(priv->window);
    GtkWidget *parent_nav = gtk_widget_get_parent(eog_nav);

    GList *children = gtk_container_get_children(GTK_CONTAINER (parent_nav));
    priv->folder_info = g_list_first(children)->data;
    g_list_free (children);

    GtkWidget *sw_view = gtk_widget_get_parent (priv->view_manager);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(sw_view), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

    return G_SOURCE_REMOVE;
}

static void
eog_job_manager_load_cb (EogJobManager *job, gpointer data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(data));
    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(data)->priv;
    
    if (priv->store_manager != NULL) {
        if (priv->signal_updated != 0)
            g_signal_handler_disconnect(priv->store_manager, priv->signal_updated);

        g_object_unref(priv->store_manager);
    }

    if (!g_slist_length (job->folder))
      return;

    priv->store_manager = GTK_LIST_STORE(job->store);
    GSList *it;
    for (it = job->folder; it != NULL; it = it->next) {
        folderContent *folder = (folderContent*)it->data;
        eog_images_manager_store_add_folder(EOG_IMAGES_MANAGER_STORE(priv->store_manager), folder->path_folder, folder->folder_name, folder->icon_folder, folder->icon_name);
    }

    eog_thumb_view_set_model(EOG_THUMB_VIEW(priv->view_manager), EOG_LIST_STORE(priv->store_manager));

    eog_images_manager_store_set_list_store(EOG_IMAGES_MANAGER_STORE(priv->store_manager), job->file_list);

    store_loaded_cb (priv->store_manager, data);

    if (!priv->from_eog){
      gtk_icon_view_set_model (GTK_ICON_VIEW (eog_window_get_thumb_view (priv->window)), NULL);
      test_unset (EOG_IMAGES_MANAGER_NAV(data));
    } /* else */
      /* g_signal_emit(EOG_IMAGES_MANAGER_NAV(data), signals[SIGNAL_IMAGES_LOADING], 0, FALSE); */
    
    priv->signal_updated = g_signal_connect (priv->store_manager, "folder-updated", G_CALLBACK (store_updated_cb), data);
    return;
}

void
eog_images_manager_nav_fill (EogImagesManagerNav *nav)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(nav));
    EogImagesManagerNavPrivate *priv = nav->priv;

    if (priv->sort == SORTED_BY_DATE) {

        if (priv->monitors_dates)
            g_hash_table_remove_all(priv->monitors_dates);
    }

    if (priv->job_manager && !eog_job_is_finished(priv->job_manager)) {
        eog_job_cancel(priv->job_manager);
        g_object_unref (priv->job_manager);
    }

    priv->job_manager = eog_job_manager_new(priv->sort, NULL, NULL, priv->mimetypes->str);
    g_signal_connect (priv->job_manager,"finished", G_CALLBACK (eog_job_manager_load_cb), nav);
//    eog_job_scheduler_add_job (priv->job_manager);
    eog_images_manager_scheduler_add_job (priv->job_manager);

}

GtkWidget*
eog_images_manager_nav_get_thumb_nav (EogImagesManagerNav *nav)
{
  g_return_val_if_fail (EOG_IS_IMAGES_MANAGER_NAV(nav), NULL);
  return nav->priv->eog_thumbnav;
}

static void
eog_images_manager_nav_constructed (GObject *object)
{
    EogImagesManagerNav *nav = EOG_IMAGES_MANAGER_NAV (object);
    EogImagesManagerNavPrivate *priv = nav->priv;

    if (G_OBJECT_CLASS (eog_images_manager_nav_parent_class)->constructed)
        G_OBJECT_CLASS (eog_images_manager_nav_parent_class)->constructed (object);

    /* For store to get images with the valid mimetypes */
    priv->mimetypes = g_string_new(NULL);
    GList *l_mimetype = eog_image_get_supported_mime_types();
    GList *it;
    for(it = l_mimetype; it != NULL; it = it->next)
        g_string_append_printf(priv->mimetypes, "'%s', ", (gchar*)it->data);

    // Temporary, we will add more mimetype (like psd)
    g_string_erase (priv->mimetypes, priv->mimetypes->len-2, 1);

    priv->job_manager = NULL;
    priv->id_job_dates = NULL;
    //    eog_images_manager_nav_fill (nav);
    priv->eog_thumbnav = eog_thumb_nav_new (priv->view_manager, EOG_THUMB_NAV_MODE_MULTIPLE_ROWS, TRUE);
    
					    //    priv->view_manager = NULL;
					    //    g_object_get (nav, "thumbview", &priv->view_manager, NULL);
    GList *cells = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT(priv->view_manager));
    GtkCellRenderer *renderer = g_list_first (cells)->data;
    g_assert (GTK_IS_CELL_RENDERER_PIXBUF(renderer));

    gtk_cell_renderer_set_fixed_size(g_list_first (cells)->data, 90, 90);
    g_list_free (cells);

    priv->signal_selection = g_signal_connect (priv->view_manager, "selection_changed", G_CALLBACK (select_folder_cb), nav);
    g_signal_connect (priv->view_manager, "item-activated", G_CALLBACK (activate_folder_cb), nav);

    priv->main_toolbar = eog_images_manager_toolbar_new(EOG_THUMB_NAV(priv->eog_thumbnav), priv->window, GTK_STACK (priv->stack_toolbars));//gtk_stack_get_child_by_name(GTK_STACK(priv->stack_toolbars), "toolbar-manager");
    gtk_stack_add_named(GTK_STACK(priv->stack_toolbars), priv->main_toolbar, "toolbar-manager");
    gtk_widget_show_all (priv->main_toolbar);

    GList *buttons = gtk_container_get_children (GTK_CONTAINER(priv->buttonbox));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_list_nth_data(buttons, priv->sort-1)), TRUE);
    g_list_free (buttons);

    gtk_box_pack_start (GTK_BOX (nav), priv->eog_thumbnav, TRUE, TRUE, 0);
    //    gtk_container_add (GTK_CONTAINER (nav), priv->eog_thumbnav);

    gtk_widget_show_all (priv->eog_thumbnav);
    g_idle_add((GSourceFunc)configure_nav_cb, nav);

    return;
}

static void
eog_images_manager_nav_set_property (GObject    *object,
                                     guint       prop_id,
                                     const GValue     *value,
                                     GParamSpec *pspec)
{
    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_get_object(value));
            break;
        case PROP_STACK:
            priv->stack_toolbars = GTK_WIDGET(g_value_get_object(value));
            break;
        case PROP_EOG:
            priv->from_eog = g_value_get_boolean (value);
            break;
        case PROP_THUMBVIEW:
	    priv->view_manager = GTK_WIDGET (g_value_get_object(value));
	    break;
        case PROP_SORT:
            priv->sort = g_value_get_uint(value);
            break;
        case PROP_ITEM_SELECTED:
            priv->name_folder = g_value_dup_string(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_images_manager_nav_dispose(GObject *object)
{
    g_return_if_fail(object != NULL);
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_NAV(object));

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(object)->priv;

//    if(priv->monitors_dates != NULL) {
//        g_hash_table_remove_all(priv->monitors_dates);
//        g_hash_table_unref(priv->monitors_dates);
//    }

    if (priv->view_manager != NULL) {
        g_signal_handler_disconnect(priv->view_manager, priv->signal_selection);
//        g_signal_handlers_disconnect_by_func(priv->view_manager, G_CALLBACK(select_folder_cb), object);
        g_signal_handlers_disconnect_by_func(priv->view_manager, G_CALLBACK(activate_folder_cb), object);
        g_clear_object(&priv->view_manager);
    }

    if(priv->store_manager != NULL) {
        g_signal_handler_disconnect(priv->store_manager, priv->signal_updated);
//        g_signal_handlers_disconnect_by_func(priv->store_manager, store_loaded_cb, object);
//        g_signal_handlers_disconnect_by_func(priv->store_manager, store_updated_cb, object);
        g_object_unref(priv->store_manager);
        priv->store_manager = NULL;
    }

    G_OBJECT_CLASS(eog_images_manager_nav_parent_class)->dispose(object);

    return;
}

static void
eog_images_manager_nav_class_init (EogImagesManagerNavClass *class )
{
    GObjectClass *g_object_class = G_OBJECT_CLASS (class);
    g_object_class->constructed = eog_images_manager_nav_constructed;
    g_object_class->dispose = eog_images_manager_nav_dispose;
    g_object_class->set_property = eog_images_manager_nav_set_property;

    signals[SIGNAL_IMAGES_LOADING] =
            g_signal_new ("images-loading",
                          EOG_TYPE_IMAGES_MANAGER_NAV,
                          G_SIGNAL_RUN_FIRST,
                          G_STRUCT_OFFSET (EogImagesManagerNavClass, images_loading),
                          NULL, NULL,
                          g_cclosure_marshal_VOID__BOOLEAN,
                          G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

    g_object_class_install_property(g_object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW,
                                                                                     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(g_object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_WIDGET,
                                                                                    G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(g_object_class, PROP_SORT, g_param_spec_uint("sort", "", "",
                                                                                 1, 3, 1,
                                                                                 G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(g_object_class, PROP_EOG, g_param_spec_boolean("from-eog", "", "",
                                                                                 FALSE,
                                                                                 G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(g_object_class, PROP_THUMBVIEW, g_param_spec_object("eog-thumbview", "", "", EOG_TYPE_THUMB_VIEW,
											G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
    
    g_object_class_install_property(g_object_class, PROP_ITEM_SELECTED, g_param_spec_string("pos-item", "", "", NULL,
                                                                                            G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
    bindtextdomain (EIM_PACKAGE, EIM_LANG_DIR);
    bind_textdomain_codeset (EIM_PACKAGE, "UTF-8");
    textdomain (EIM_PACKAGE);

    return;
}

static gboolean
sort_button_clicked_cb (GtkWidget *widget,
                        GdkEvent  *event,
                        EogImagesManagerNav *nav)
{
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) == TRUE)
        return TRUE;
    else
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), TRUE);

    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_NAV(nav), FALSE);

    EogImagesManagerNavPrivate *priv = nav->priv;

    GSList *l_radio = gtk_radio_button_get_group(GTK_RADIO_BUTTON(widget));

    priv->sort = g_slist_index(l_radio, widget)+1;

    gboolean set_items = (priv->sort == SORTED_BY_FOLDERS_NAME) ? TRUE : FALSE;
    eog_images_manager_toolbar_hide_items(EOG_IMAGES_MANAGER_TOOLBAR(priv->main_toolbar), TRUE, set_items);

    gtk_tree_path_free (priv->folder_selected);
    priv->folder_selected = gtk_tree_path_new_first();

    if (priv->signal_updated) {
        g_signal_handler_disconnect(priv->store_manager, priv->signal_updated);
        priv->signal_updated = 0;
    }

    g_signal_emit(nav, signals[SIGNAL_IMAGES_LOADING], 0, TRUE);
    eog_images_manager_nav_fill (nav);
    return FALSE;
}

static void
eog_images_manager_nav_init (EogImagesManagerNav *nav)
{
    nav->priv = eog_images_manager_nav_get_instance_private(nav);
    EogImagesManagerNavPrivate *priv = nav->priv;

    gtk_orientable_set_orientation (GTK_ORIENTABLE (nav),
				    GTK_ORIENTATION_VERTICAL);

    priv->store_empty = FALSE;
    // Adding button box

    GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    GtkWidget *buttonbox = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(buttonbox), GTK_BUTTONBOX_EXPAND);

    GtkWidget *label = gtk_label_new(D_("Sorted by : "));
    gtk_label_set_xalign (GTK_LABEL (label), 0);//0.1);
    gtk_box_pack_start (GTK_BOX(vbox), label, FALSE, FALSE, 10);

//    GtkWidget *button_alpha = gtk_radio_button_new_with_label(NULL, "Alphabets");
//    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button_alpha), FALSE);
//    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button_alpha), FALSE);

    GtkWidget *button_dates = gtk_radio_button_new_with_label(NULL, D_("Month"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button_dates), FALSE);
    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button_dates), FALSE);

    GtkWidget *button_folders = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(button_dates), D_("Folders"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button_folders), FALSE);
    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button_folders), FALSE);

    gtk_container_add (GTK_CONTAINER (buttonbox), button_folders);
    gtk_container_add (GTK_CONTAINER (buttonbox), button_dates);
//    gtk_container_add (GTK_CONTAINER (buttonbox), button_alpha);

    g_signal_connect(GTK_RADIO_BUTTON(button_folders), "button-press-event", G_CALLBACK(sort_button_clicked_cb), nav);
    g_signal_connect(GTK_RADIO_BUTTON(button_dates), "button-press-event", G_CALLBACK(sort_button_clicked_cb), nav);
//    g_signal_connect(GTK_RADIO_BUTTON(button_alpha), "button-press-event", G_CALLBACK(sort_button_clicked_cb), plugin);

    gtk_box_pack_start (GTK_BOX(vbox), buttonbox, FALSE, FALSE, 0);
    gtk_container_add(GTK_CONTAINER(nav), vbox);

//    gtk_widget_show_all (vbox);
//    gtk_box_reorder_child(GTK_BOX(nav), vbox, 0);

    priv->buttonbox = buttonbox;

    priv->monitors_dates = NULL;

    priv->gallery_settings = g_settings_new (EOG_CONF_UI);
    priv->display_gallery = FALSE;

    priv->label_folder = D_("Album");
    priv->len_images = D_("photo");
    priv->home_folder = D_("Various");
    priv->screensaver = D_("Screensaver");
    return;
}

GtkWidget *
eog_images_manager_nav_new (EogWindow *window,
                            GtkWidget *stack,
                            GtkWidget *thumb_view,
			    gboolean from_eog,
                            guint sort,
                            const gchar *folder)
{
    return g_object_new(EOG_TYPE_IMAGES_MANAGER_NAV,
                        "window", window,
                        "stack", stack,
                        "eog-thumbview", thumb_view,
			"from-eog", from_eog,
                        "sort", sort,
                        "pos-item", folder,
                        NULL);
}

guint
eog_images_manager_nav_get_sort (EogImagesManagerNav *nav)
{
    g_return_val_if_fail (EOG_IS_IMAGES_MANAGER_NAV(nav), 0);

    return nav->priv->sort;
}

static void
eog_job_manager_test_cb (EogJobManager *job, gpointer data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(data));
    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(data)->priv;
    
    if (priv->store_manager != NULL) {
        if (priv->signal_updated != 0)
            g_signal_handler_disconnect(priv->store_manager, priv->signal_updated);

        g_object_unref(priv->store_manager);
    }

    if (!g_slist_length (job->folder))
      return;

    priv->store_manager = GTK_LIST_STORE(job->store);
    g_signal_handlers_block_by_func (priv->view_manager, activate_folder_cb, data);
    GSList *it;
    for (it = job->folder; it != NULL; it = it->next) {
        folderContent *folder = (folderContent*)it->data;
        eog_images_manager_store_add_folder(EOG_IMAGES_MANAGER_STORE(priv->store_manager), folder->path_folder, folder->folder_name, folder->icon_folder, folder->icon_name);
    }

    gchar *tmp = g_strdup (priv->name_folder);
    g_free (priv->name_folder);

    eog_thumb_view_set_model(EOG_THUMB_VIEW(priv->view_manager), EOG_LIST_STORE(priv->store_manager));
    //gtk_icon_view_set_model(GTK_ICON_VIEW(priv->view_manager), GTK_TREE_MODEL(priv->store_manager));
    eog_images_manager_store_set_list_store(EOG_IMAGES_MANAGER_STORE(priv->store_manager), job->file_list);
    
    priv->name_folder = g_strdup (tmp);

    select_folder_by_name (EOG_IMAGES_MANAGER_NAV(data));

    priv->name_folder = g_strdup (tmp);
    g_free (tmp);

    priv->signal_updated = g_signal_connect (priv->store_manager, "folder-updated", G_CALLBACK (store_updated_cb), data);
    g_signal_handlers_unblock_by_func (priv->view_manager, activate_folder_cb, data);
    return;
}

void
eog_images_manager_nav_get_images_from_folder (EogImagesManagerNav *nav,
					       guint                sort,
					       gchar               *name_folder)
{

  g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(nav));
  g_return_if_fail (name_folder != NULL);

  EogImagesManagerNavPrivate *priv = nav->priv;
  EogListStore *store = eog_window_get_store (priv->window);
  
  if (priv->name_folder != NULL) {
    g_free (priv->name_folder);
    priv->name_folder = NULL;
  }

  if (priv->image_added != 0) {
    g_signal_handler_disconnect(GTK_TREE_MODEL (store), priv->image_added);
    priv->image_added = 0;
  }
  if (priv->image_removed != 0) {
    g_signal_handler_disconnect(GTK_TREE_MODEL (store), priv->image_removed);
    priv->image_removed = 0;
  }

  if (priv->signal_updated != 0) {
    g_signal_handler_disconnect(priv->store_manager, priv->signal_updated);
    priv->signal_updated = 0;
  }

  if (priv->monitors_dates)
    g_hash_table_remove_all(priv->monitors_dates);

  gtk_tree_path_free (priv->folder_selected);
  
  priv->name_folder = g_strdup (name_folder);
  if (priv->sort == SORTED_BY_FOLDERS_NAME) {
    gchar *tmp = g_strdup (name_folder);
    select_folder_by_name (nav);
    priv->name_folder = tmp;
    return;
  }
  
  GList *buttons = gtk_container_get_children (GTK_CONTAINER(priv->buttonbox));
  /* TODO : change to get the sort in consideration. For now no use since we use for handle new instance. */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_list_first(buttons)->data), TRUE);
  g_list_free (buttons);
  
  priv->sort = sort;
    
  gboolean set_items = (priv->sort == SORTED_BY_FOLDERS_NAME) ? TRUE : FALSE;
  eog_images_manager_toolbar_hide_items(EOG_IMAGES_MANAGER_TOOLBAR(priv->main_toolbar), TRUE, set_items);

  priv->folder_selected = gtk_tree_path_new_first();

  if (priv->job_manager && !eog_job_is_finished(priv->job_manager)) {
    eog_job_cancel(priv->job_manager);
    g_object_unref (priv->job_manager);
  }
  
  priv->job_manager = eog_job_manager_new(priv->sort, NULL, NULL, priv->mimetypes->str);
  g_signal_connect (priv->job_manager,"finished", G_CALLBACK (eog_job_manager_test_cb), nav);
  eog_images_manager_scheduler_add_job (priv->job_manager);

  return;
}

/*
 * Funcs for images in the store of eog
 */
static void
destroy_monitors (gpointer data)
{
    g_file_monitor_cancel (G_FILE_MONITOR (data));
}

static void
eog_job_images_finished_cb (EogJobGetImages *job, gpointer data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(data));

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(data)->priv;

    GError *error = NULL;
    GSList *it;
    for (it = job->file_list; it != NULL; it = it->next) {
        GFile *file_path = (GFile*)it->data;
        GFileMonitor *monitor = g_file_monitor_file(file_path, G_FILE_MONITOR_WATCH_MOVES, NULL, &error);
        if(error){
            g_printerr("Cannot monitoring file %s : %s\n", g_file_get_parse_name(file_path), error ? error->message : "Unknown error");
            g_clear_error(&error);
        } else if (monitor) {
            g_signal_connect(monitor, "changed", G_CALLBACK(dates_monitors_gallery_cb), data);
            g_hash_table_insert(priv->monitors_dates, g_strdup(g_file_get_parse_name(file_path)), monitor);
        }
    }

    eog_window_open_file_list(EOG_WINDOW(priv->window), job->file_list);
    return;
}

static void
eog_job_images_update_cb (EogJobGetImages *job, gpointer data)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_NAV(data));

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(data)->priv;

    /*
    if (!job->file_list) {
        g_warning ("List empty, we return");
        return;
    }*/

    GSList *it;
    GError *error = NULL;
    for (it = job->file_list; it != NULL; it = it->next) {
        GFile *file_path = (GFile *) it->data;
        if (!g_hash_table_contains(priv->monitors_dates, g_file_get_parse_name(file_path))) {
            GFileMonitor *monitor = g_file_monitor_file(file_path, G_FILE_MONITOR_WATCH_MOVES, NULL, &error);
            if (error) {
                g_printerr("Cannot monitoring file %s : %s\n", g_file_get_parse_name(file_path),
                           error ? error->message : "Unknown error");
                g_clear_error(&error);
            } else if (monitor) {
                g_signal_connect(monitor, "changed", G_CALLBACK(dates_monitors_gallery_cb), data);
                g_hash_table_insert(priv->monitors_dates, g_strdup(g_file_get_parse_name(file_path)), monitor);
//                gchar *caption = g_file_get_basename(file_path);
//                EogImage *new_image = eog_image_new_file(file_path, caption);
//                eog_list_store_append_image(EOG_LIST_STORE(eog_window_get_store(priv->window)), new_image);
            }
        }
    }

    priv->id_job_dates = NULL;
    g_slist_foreach(job->file_list, (GFunc)g_object_unref, NULL);
    g_slist_free(job->file_list);
}

static void
eog_images_manager_nav_update_gallery (EogImagesManagerNav *nav)
{
    /*
    if (nav->priv->id_job_dates != NULL) {
        g_message ("Tried to start job twice");
        return;
    }*/

    GString* string_path = g_string_new(NULL);
    GList* it;
    GList* current_list = g_hash_table_get_keys(nav->priv->monitors_dates);
    for (it = current_list; it != NULL; it = it->next) {
        if (it->next == NULL)
            g_string_append_printf(string_path, "\"%s\"", (gchar*)it->data);
        else
            g_string_append_printf(string_path, "\"%s\", ", (gchar*)it->data);
    }

    nav->priv->id_job_dates = eog_job_get_images_new(nav->priv->mimetypes->str, nav->priv->name_folder, TRUE, eog_window_get_store(nav->priv->window), g_strdup(string_path->str));
    g_signal_connect (nav->priv->id_job_dates,"finished", G_CALLBACK (eog_job_images_update_cb), nav);
//    eog_job_scheduler_add_job(nav->priv->id_job_dates);
    eog_images_manager_scheduler_add_job (nav->priv->id_job_dates);
    g_list_free (current_list);
    g_string_free (string_path, TRUE);
    g_object_unref(nav->priv->id_job_dates);

    return;
}

/*
 * We use it for folders sorted by date. It's called mostly when the date is updated
 */
gboolean
eog_images_manager_nav_is_image_in_gallery (GtkTreeModel *gallery_model, GFile *file, GtkTreeIter *iter_found)
{
    gboolean found = FALSE;
    GtkTreeIter iter;
    EogImage *image = NULL;

    if (!gtk_tree_model_get_iter_first (gallery_model, &iter))
        return FALSE;

    do{
        gtk_tree_model_get(gallery_model, &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);

        GFile *file_image = eog_image_get_file(image);
        found = g_file_equal(file_image, file);

        g_object_unref(file_image);
    } while (!found && gtk_tree_model_iter_next(gallery_model, &iter));

    if(found && iter_found != NULL)
        *iter_found = iter;
    return found;
}

static void
dates_monitors_gallery_cb (GFileMonitor *monitor, GFile *file, GFile *other_file, GFileMonitorEvent event, EogImagesManagerNav *nav)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_NAV(nav));
    EogImagesManagerNavPrivate *priv = nav->priv;

    EogThumbView *thumbView = EOG_THUMB_VIEW(eog_window_get_thumb_view(priv->window));
    EogListStore *gallery = EOG_LIST_STORE(gtk_icon_view_get_model(GTK_ICON_VIEW(thumbView)));
    g_return_if_fail(EOG_IS_LIST_STORE(gallery));

    GtkTreeIter iter;
    EogImage *image;

    switch (event){
        case G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT:
            if(eog_images_manager_nav_is_image_in_gallery(GTK_TREE_MODEL(gallery),  file, &iter)) {
                gtk_tree_model_get(GTK_TREE_MODEL(gallery), &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
                eog_image_file_changed(image);

                g_object_unref (image);

                // Without it, the drawing area is white
                eog_thumb_view_select_single (EOG_THUMB_VIEW (thumbView),
                                              EOG_THUMB_VIEW_SELECT_RIGHT);
                eog_thumb_view_select_single (EOG_THUMB_VIEW (thumbView),
                                              EOG_THUMB_VIEW_SELECT_LEFT);

                eog_list_store_thumbnail_refresh(gallery, &iter);
            }
            break;
        case G_FILE_MONITOR_EVENT_RENAMED:
            if(eog_images_manager_nav_is_image_in_gallery(GTK_TREE_MODEL(gallery),  file, &iter)) {
                gtk_tree_model_get(GTK_TREE_MODEL(gallery), &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
                g_object_unref(image);
                image = eog_image_new_file(other_file, g_file_get_basename(other_file));
                gtk_list_store_set(GTK_LIST_STORE(gallery), &iter, EOG_LIST_STORE_EOG_IMAGE, image, -1);
            }
        case G_FILE_MONITOR_EVENT_MOVED_OUT:
            if (other_file == NULL || (g_file_query_file_type (other_file, 0, NULL) == G_FILE_TYPE_MOUNTABLE) ){
                if(eog_images_manager_nav_is_image_in_gallery(GTK_TREE_MODEL(gallery),  file, &iter)){
                    gtk_tree_model_get(GTK_TREE_MODEL(gallery), &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
                    if(image == eog_thumb_view_get_first_selected_image(thumbView)) {
                        eog_thumb_view_select_single(EOG_THUMB_VIEW(thumbView), EOG_THUMB_VIEW_SELECT_LEFT);
                    }

		    eog_list_store_remove_image (EOG_LIST_STORE (gallery), image);

                    if (g_hash_table_remove(priv->monitors_dates, g_file_get_parse_name(file)))
                        g_message ("Removed monitor for the file %s", g_file_get_parse_name(file));

                    if (eog_list_store_length(gallery) == 1) {
                        eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_window_get_thumb_nav(priv->window)), EOG_THUMB_NAV_MODE_ONE_ROW);
                    }
                }
            }
            break;
        case G_FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED:
        case G_FILE_MONITOR_EVENT_DELETED:
        case G_FILE_MONITOR_EVENT_PRE_UNMOUNT:
        case G_FILE_MONITOR_EVENT_UNMOUNTED:
        case G_FILE_MONITOR_EVENT_CREATED:
        case G_FILE_MONITOR_EVENT_MOVED_IN:
        case G_FILE_MONITOR_EVENT_MOVED:
        case G_FILE_MONITOR_EVENT_CHANGED:
        default:
            break;
    }
    return;
}

static void
eog_images_manager_nav_get_images_by_dates (EogImagesManagerNav *nav)
{
    EogJob *job = NULL;
    job = eog_job_get_images_new(nav->priv->mimetypes->str, nav->priv->name_folder, FALSE, NULL, NULL);
    g_signal_connect (job,"finished", G_CALLBACK (eog_job_images_finished_cb), nav);
//    eog_job_scheduler_add_job(job);
    eog_images_manager_scheduler_add_job(job);
    g_object_unref(job);
}


static void
len_images_updated (EogImagesManagerNav *nav)
{
    EogImagesManagerNavPrivate *priv = nav->priv;

    EogListStore *store = eog_window_get_store (priv->window);
    guint len = eog_list_store_length (store);

    gchar *len_images = g_strdup_printf("%d %s%s", len, priv->len_images, len == 1 ? "" : "s");

    gchar *info = g_strconcat(priv->label_album, "\" (", len_images, ")", NULL);
    gtk_label_set_text(GTK_LABEL (priv->folder_info), info);
    g_free (info);
}

static gboolean
update_monitors (gpointer key, gpointer value, gpointer data)
{
  GFile *file = g_file_new_for_path ( (gchar*)key);

  if (!g_file_query_exists (file, NULL)) {
    g_message ("File %s doesn't existed, removed from the hash_table", g_file_get_uri (file));
    return TRUE;
  }
  return FALSE;
}

static void
images_deleted (GtkTreeModel *tree_model,
                GtkTreePath  *path,
                gpointer      user_data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_NAV(user_data));

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(user_data)->priv;
    len_images_updated (EOG_IMAGES_MANAGER_NAV(user_data));

    g_hash_table_foreach_remove (priv->monitors_dates, update_monitors, NULL);
    return;
}

static void
images_inserted (GtkTreeModel *tree_model,
                 GtkTreePath  *path,
                 GtkTreeIter  *iter,
                 gpointer      user_data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_NAV(user_data));

    len_images_updated (EOG_IMAGES_MANAGER_NAV(user_data));
    return;
}

void
eog_images_manager_nav_update_label_folder (EogImagesManagerNav *nav)
{
  EogImagesManagerNavPrivate *priv = nav->priv;

    EogListStore *store = eog_window_get_store (priv->window);
    if (store) {
      priv->image_added = g_signal_connect (GTK_TREE_MODEL (store), "row-inserted", G_CALLBACK (images_inserted), nav);
      priv->image_removed = g_signal_connect (GTK_TREE_MODEL (store), "row-deleted", G_CALLBACK (images_deleted), nav);
    }
    guint len = eog_list_store_length (store);
    gchar *len_images = g_strdup_printf("%d %s%s", len, priv->len_images, len == 1 ? "" : "s");
    gchar *label_folder = g_strconcat(priv->label_folder, " \"", NULL);

    gchar *info = NULL;
    if (priv->sort == SORTED_BY_FOLDERS_NAME) {
      const gchar *home_path = g_get_home_dir();
      gchar *path_folder = g_filename_from_uri(priv->name_folder, NULL, NULL);
      if (!strcmp(path_folder, home_path)) {
	label_folder = g_strconcat(label_folder, priv->home_folder, NULL);
      } else if (!strcmp (g_path_get_basename(priv->name_folder), "Photos_de_veilles")) {
	//            gchar *new_path = NULL;
	//            if(!strcmp (g_path_get_dirname(path_folder), home_path))
	//                new_path = g_strconcat(g_strdup (priv->home_folder), "/", priv->screensaver, NULL);
	//            else{
	//                new_path = g_strconcat(g_path_get_dirname(path_folder), "/", priv->screensaver, NULL);
	//                new_path = new_path + strlen(home_path) + 1;
	//            }
	//
	//            label_folder = g_strconcat(label_folder, new_path, NULL);
	label_folder = g_strconcat(label_folder, priv->screensaver, NULL);
      } else
	label_folder = g_strconcat(label_folder, g_path_get_basename(path_folder), NULL);
      //            label_folder = g_strconcat(label_folder, path_folder + strlen(home_path) + 1, NULL);

      info = g_strconcat(label_folder, "\" (", len_images, ")", NULL);
    } else {
      GDate date_folder;
      gchar **year_month = g_strsplit(priv->name_folder, "-", -1);
      gchar *date_str = g_strconcat(year_month[1], " ", year_month[0], NULL);
      g_date_set_parse (&date_folder, date_str);
      if (g_date_valid (&date_folder)){
	gchar month_year[80];
	g_date_strftime (month_year, 80, "%B %Y", &date_folder);
	month_year[0] = g_ascii_toupper(month_year[0]);
	label_folder = g_strconcat(label_folder, month_year, NULL);
	info = g_strconcat(label_folder, "\" ", "(", len_images, ")", NULL);
	g_free (date_str);
	g_strfreev (year_month);
      } else {
	info = g_strconcat(label_folder, priv->name_folder, "\" ", "(", len_images, ")", NULL);
      }
    }
    if (priv->label_album)
      g_free (priv->label_album);

    priv->label_album = g_strdup (label_folder);
    gtk_label_set_text(GTK_LABEL (priv->folder_info), info);
    g_free (info);
    g_free (len_images);
    g_free (label_folder);

    return;
}

static void
images_folder_loaded (EogWindow *window, gpointer user_data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_NAV(user_data));

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(user_data)->priv;

    if (priv->id_load_gallery > 0) {
        g_signal_handler_disconnect(window, priv->id_load_gallery);
        priv->id_load_gallery = 0;
    }

    eog_images_manager_nav_update_label_folder (EOG_IMAGES_MANAGER_NAV (user_data));
    //    set_label_folder (EOG_IMAGES_MANAGER_NAV (user_data));

    EogListStore *store = eog_window_get_store (window);
    guint len = eog_list_store_length (store);
    // Boolean to avoid displaying gallery the first time
    GtkWidget *eog_thumbview = eog_window_get_thumb_view (window);

    if (!priv->display_gallery) {
        priv->display_gallery = TRUE;
        g_signal_emit(user_data, signals[SIGNAL_IMAGES_LOADING], 0, FALSE);
        return;
    }

    GtkWidget *eog_nav = eog_window_get_thumb_nav (window);
    EogThumbNavMode eog_mode = eog_thumb_nav_get_mode (EOG_THUMB_NAV (eog_nav));

    if (len > 1) {
        if (eog_mode == EOG_THUMB_NAV_MODE_ONE_ROW)
            eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);
        g_timeout_add(1000, resize_thumbnails_idle, user_data);
	//        eog_utils_resize_thumbnails(EOG_THUMB_NAV (eog_nav), EOG_THUMB_VIEW (eog_thumbview), g_settings_get_enum (priv->gallery_settings, GALLERY_SIZE));
        gtk_icon_view_unselect_all(GTK_ICON_VIEW (eog_thumbview));
        gtk_stack_set_visible_child_name(GTK_STACK(priv->stack_toolbars), "toolbar-manager");
    } else {
        if (eog_mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
            eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_ONE_ROW);
        }
        g_object_set (store, "size-thumbnail", 90, NULL);
        gtk_stack_set_visible_child_name(GTK_STACK(priv->stack_toolbars), "main-toolbar");
    }

//    g_message ("TEXT : %s", gtk_label_get_label(GTK_LABEL (priv->folder_info)));

    GtkWidget *eog_scrollview = eog_window_get_view (priv->window);
    GtkWidget *box_info = gtk_grid_get_child_at(GTK_GRID (eog_scrollview), 0, 2);
    if (box_info && gtk_widget_get_visible (box_info))
      gtk_widget_hide (box_info);

    g_signal_emit(user_data, signals[SIGNAL_IMAGES_LOADING], 0, FALSE);

    return;
}

static gboolean
eog_images_manager_nav_get_images (gpointer nav)//EogImagesManagerNav *nav)
{
  g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_NAV(nav), G_SOURCE_REMOVE);

    EogImagesManagerNavPrivate *priv = EOG_IMAGES_MANAGER_NAV(nav)->priv;

//    GObject *eog_thumbview = G_OBJECT (eog_window_get_thumb_view(priv->window));
//    gtk_icon_view_set_model(GTK_ICON_VIEW (eog_thumbview), NULL);

    EogListStore *store = eog_window_get_store (priv->window);
    if (priv->image_added) {
        g_signal_handler_disconnect(GTK_TREE_MODEL (store), priv->image_added);
	priv->image_added = 0;
    }
    if (priv->image_removed) {
        g_signal_handler_disconnect(GTK_TREE_MODEL (store), priv->image_removed);
	priv->image_removed = 0;
    }
	
    priv->id_load_gallery = g_signal_connect (priv->window, "prepared", G_CALLBACK(images_folder_loaded), nav);

    if(priv->sort == SORTED_BY_FOLDERS_NAME){
        eog_list_store_set_monitoring(TRUE);

        GFile *folder = g_file_new_for_uri(priv->name_folder);
        GFileInfo *folder_info = g_file_query_info(folder, G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE,
                                                   G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS, NULL, NULL);
        gboolean is_writable = g_file_info_get_attribute_boolean(folder_info, G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE);

        eog_images_manager_toolbar_hide_items(EOG_IMAGES_MANAGER_TOOLBAR(priv->main_toolbar), is_writable, is_writable);

        g_object_unref(folder);
        g_object_unref(folder_info);

        eog_window_open_file_list(priv->window, g_slist_append(NULL, g_file_new_for_uri(priv->name_folder)));
    } else {
        eog_list_store_set_monitoring(FALSE);
        if (!priv->monitors_dates) {
            priv->monitors_dates = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, destroy_monitors);
        } else
            g_hash_table_remove_all(priv->monitors_dates);
        eog_images_manager_nav_get_images_by_dates(nav);
    }
    return G_SOURCE_REMOVE;
}
