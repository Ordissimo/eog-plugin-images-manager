/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#include <glib/gi18n.h>
#include "eog-utils.h"
#include <eog-3.0/eog/eog-thumb-view.h>
#include <libtracker-sparql/tracker-sparql.h>
#include "eog-images-manager-toolbar.h"
#include "eog-images-manager-store.h"

struct _EogImagesManagerToolbarPrivate {
    EogThumbNav *nav;
    EogWindow *window;
    GtkStack *stack;

    GtkToolItem *rename_item;
    GtkToolItem *delete_item;

    gchar *renamed_file;
};

enum {
    PROP_0,
    PROP_NAV,
    PROP_WINDOW,
    PROP_STACK
};

G_DEFINE_TYPE_WITH_PRIVATE (EogImagesManagerToolbar, eog_images_manager_toolbar, GTK_TYPE_TOOLBAR)

enum {
    SIGNAL_PROCESS_START,
    SIGNAL_PROCESS_STOPPED,
    SIGNAL_LAST
};
static gint signals [SIGNAL_LAST];

static void
eog_images_manager_toolbar_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_images_manager_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (eog_images_manager_toolbar_parent_class)->constructed (object);

    EogImagesManagerToolbar *toolbar = EOG_IMAGES_MANAGER_TOOLBAR(object);
    EogImagesManagerToolbarPrivate *priv = toolbar->priv;

    return;
}

static void
eog_images_manager_toolbar_dispose (GObject *object)
{

    G_OBJECT_CLASS (eog_images_manager_toolbar_parent_class)->dispose (object);
    return;
}

static void
eog_images_manager_toolbar_set_property (GObject    *object,
                               guint       prop_id,
                               const GValue     *value,
                               GParamSpec *pspec)
{
    EogImagesManagerToolbarPrivate *priv = EOG_IMAGES_MANAGER_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_NAV:
            priv->nav = EOG_THUMB_NAV (g_value_get_object(value));
            break;
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_get_object (value));
            break;
        case PROP_STACK:
            priv->stack = GTK_STACK(g_value_get_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_images_manager_toolbar_class_init (EogImagesManagerToolbarClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_images_manager_toolbar_constructed;
    object_class->dispose = eog_images_manager_toolbar_dispose;
    object_class->set_property = eog_images_manager_toolbar_set_property;

    g_object_class_install_property(object_class, PROP_NAV, g_param_spec_object("nav", "", "", EOG_TYPE_THUMB_NAV, (G_PARAM_WRITABLE |
                                                                                                                       G_PARAM_CONSTRUCT_ONLY |
                                                                                                                       G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW, (G_PARAM_WRITABLE |
                                                                                                                       G_PARAM_CONSTRUCT_ONLY |
                                                                                                                       G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_STACK, (G_PARAM_WRITABLE |
                                                                                                                    G_PARAM_CONSTRUCT_ONLY |
                                                                                                                    G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));

    signals[SIGNAL_PROCESS_START] = g_signal_new ("process-started",
                                                  EOG_TYPE_IMAGES_MANAGER_TOOLBAR,
                                                  G_SIGNAL_RUN_FIRST,
                                                  G_STRUCT_OFFSET (EogImagesManagerToolbarClass , process_started),
                                                  NULL, NULL,
                                                  g_cclosure_marshal_VOID__VOID,
                                                  G_TYPE_NONE,
                                                  0);

    signals[SIGNAL_PROCESS_STOPPED] = g_signal_new ("process-stopped",
                                                    EOG_TYPE_IMAGES_MANAGER_TOOLBAR,
                                                    G_SIGNAL_RUN_FIRST,
                                                    G_STRUCT_OFFSET (EogImagesManagerToolbarClass , process_stopped),
                                                    NULL, NULL,
                                                    g_cclosure_marshal_VOID__VOID,
                                                    G_TYPE_NONE,
                                                    0);
    return;
}

GtkWidget *
eog_images_manager_toolbar_new (EogThumbNav *nav, EogWindow *window, GtkStack *stack)
{
    return g_object_new (EOG_TYPE_IMAGES_MANAGER_TOOLBAR, "nav", nav, "window", window, "stack", stack, NULL);
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON SLIDESHOW
 *
 * * * * * *  * * * * * * * *  * * */
static gboolean
key_press_event_cb (GtkWidget *widget,
                    GdkEvent  *event,
                    gpointer   user_data) {
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(user_data), FALSE);
    EogImagesManagerToolbarPrivate *priv = EOG_IMAGES_MANAGER_TOOLBAR(user_data)->priv;

    switch (event->key.keyval) {
        case GDK_KEY_Escape:
            gtk_widget_set_visible(GTK_WIDGET(priv->nav), TRUE);
            eog_window_set_mode(priv->window, EOG_WINDOW_MODE_NORMAL);
            GtkWidget *boxtool = eog_window_get_boxtool(priv->window);
            if (!gtk_widget_get_visible(boxtool))
                gtk_widget_set_visible(boxtool, TRUE);

            gtk_widget_show_all(boxtool);

            GAction *action = g_action_map_lookup_action (G_ACTION_MAP (priv->window), "view-gallery");
            g_action_change_state (action, g_variant_new_boolean (TRUE));

            // Bad practice ?
            g_signal_handlers_disconnect_by_func (priv->window, key_press_event_cb, user_data);
            break;
        default:
            break;
    }
    return TRUE;
}

static void
default_slideshow (EogImagesManagerToolbar *toolbar)
{
    EogImagesManagerToolbarPrivate *priv = toolbar->priv;
    EogWindow *window = priv->window;

    if (priv->nav)
        gtk_widget_set_visible(GTK_WIDGET(priv->nav), FALSE);

    if (eog_thumb_nav_get_mode(priv->nav) == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
        GtkWidget *t_view = eog_window_get_thumb_view(priv->window);
        eog_thumb_view_select_single(EOG_THUMB_VIEW(t_view), EOG_THUMB_VIEW_SELECT_FIRST);
        g_signal_emit_by_name(t_view, "item-activated", NULL);
    }
    eog_window_set_mode (window, EOG_WINDOW_MODE_SLIDESHOW);

    g_signal_connect(GTK_WIDGET(priv->window),
                     "key-press-event",
                     G_CALLBACK(key_press_event_cb), toolbar);
    return;
}

static void
button_slideshow_cb (GtkToolButton *toolbutton,
                     gpointer       data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(data));

    EogImagesManagerToolbarPrivate *priv = EOG_IMAGES_MANAGER_TOOLBAR(data)->priv;

    gboolean oinfo_exist = g_find_program_in_path ("oinfo") != NULL ? TRUE : FALSE;
    if (oinfo_exist) {
        GtkTreeIter iter;
        GError *error = NULL;
        EogImage *image;
        GFile *file;
        gchar *path;
        GString *args = g_string_new ("photossimo ");
        GtkTreeModel *model = GTK_TREE_MODEL (eog_window_get_store (priv->window));
        GtkWidget *eog_nav = eog_window_get_thumb_nav(priv->window);
        EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV(eog_nav));
        gboolean valid = gtk_tree_model_get_iter_first(model, &iter);
        if (!valid)
            return;
        do {
            gtk_tree_model_get (model, &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
            file = eog_image_get_file(image);
            path = g_file_get_path(file);
            g_string_append_printf(args, "'%s' ", path);
        } while (gtk_tree_model_iter_next(model, &iter));
        if (path)
            g_free (path);
        if(file)
            g_object_unref (file);
        if (image)
            g_object_unref (G_OBJECT (image));

        gboolean spawned = g_spawn_command_line_async(args->str, &error);
        if (!spawned || error) {
            g_critical("Could not spawn photossimo : %s", error ? error->message : "Unknown error");
            default_slideshow(EOG_IMAGES_MANAGER_TOOLBAR(data));
        }
        g_string_free (args, TRUE);
    } else
        default_slideshow(EOG_IMAGES_MANAGER_TOOLBAR(data));
    return;
}

static GtkToolItem *
item_button_slideshow (EogImagesManagerToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar), NULL);

    GtkWidget *img_slideshow = gtk_image_new_from_icon_name("view-fullscreen", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_slideshow, D_("Slideshow"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_slideshow_cb), toolbar);

    return ss;

}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON RENAME
 *
 * * * * * *  * * * * * * * *  * * */
gchar *
check_renamed_file (const gchar *parent_path, const gchar *new_name)
{
    gchar *dir = g_path_get_dirname(parent_path);
    gchar *new_file_name = g_build_filename(dir, new_name, NULL);

    int count = 1;
    while (!access(new_file_name, F_OK)) {
        g_free (new_file_name);

        gchar *str_count = g_strdup_printf("-%i", count);
        gchar *new_name_inc = g_strconcat(new_name, str_count, NULL);

        new_file_name = g_build_filename(dir, new_name_inc, NULL);

        count++;

        g_free (str_count);
        g_free (new_name_inc);
    }

    if (rename (parent_path, new_file_name))
        return NULL;

    return new_file_name;
}

static void
rename_image (EogImagesManagerToolbar *toolbar, EogImage *image, const gchar *new_name)
{
    g_return_if_fail (EOG_IS_IMAGES_MANAGER_TOOLBAR (toolbar));

    GFile *image_file = eog_image_get_file(image);
    gchar *image_name = g_file_get_basename(image_file);

    GFile *parent_folder = g_file_get_parent (image_file);
    gchar *parent_path = g_file_get_parse_name(parent_folder);

    toolbar->priv->renamed_file = check_renamed_file(parent_path, new_name);
    if (!toolbar->priv->renamed_file) {
        g_warning("Cannot rename %s to %s", g_file_get_parse_name(parent_folder), new_name);
        return;
    }

//    gchar *new_path_image = g_build_filename(toolbar->priv->renamed_file, image_name, NULL);
//    new_path_image = g_filename_to_uri(new_path_image, NULL, NULL);

    toolbar->priv->renamed_file = g_filename_to_uri(toolbar->priv->renamed_file, NULL, NULL);
    g_warning("Renamed : %s", toolbar->priv->renamed_file);

//    parent_path = g_file_get_uri(parent_folder);
//    eog_images_manager_nav_select_folder_by_path (toolbar->priv->nav, parent_path, toolbar->priv->renamed_file, new_path_image);
//    g_timeout_add(2000, test_timer, toolbar);
    //    const gchar *new_caption = g_path_get_basename(renamed_file);
//    image_file = g_file_set_display_name (image_file, new_caption, NULL, NULL);

    return;
}

static void
button_rename_cb (GtkToolButton *toolbutton,
                  gpointer       data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(data));

    EogImagesManagerToolbarPrivate *priv = EOG_IMAGES_MANAGER_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

/*
    EogImage *curr_folder = eog_images_manager_nav_get_folder_selected(priv->nav);
    if(curr_folder == NULL)
        return;

    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(window),GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                               GTK_MESSAGE_QUESTION, GTK_BUTTONS_OK_CANCEL,
                                               _("Enter the new name :"));
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG (dialog));
    GtkWidget *entry = gtk_entry_new();
    gtk_entry_set_activates_default(GTK_ENTRY(entry), TRUE);

    gchar *caption = g_strdup(eog_image_get_caption(curr_folder));

    gtk_entry_set_text(GTK_ENTRY(entry), caption);
    gtk_container_add (GTK_CONTAINER (content_area), entry);
    gtk_widget_show_all(dialog);

    int result = gtk_dialog_run(GTK_DIALOG(dialog));
    switch (result){
        case GTK_RESPONSE_OK:
            rename_image (EOG_IMAGES_MANAGER_TOOLBAR(data), curr_folder, gtk_entry_get_text(GTK_ENTRY (entry)));
            break;
        default:
            break;
    }

    if (caption)
        g_free(caption);

    gtk_widget_destroy(dialog);
*/
    return;
}

static GtkToolItem *
item_button_rename (EogImagesManagerToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar), NULL);

    GtkWidget *img_rename = gtk_image_new_from_icon_name("format-text-bold", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *rename = gtk_tool_button_new(img_rename, D_("Rename"));

    g_signal_connect(GTK_TOOL_BUTTON(rename), "clicked", G_CALLBACK(button_rename_cb), toolbar);

    return rename;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON PRINT
 *
 * * * * * *  * * * * * * * *  * * */
static pid_t
pid_impressimo (void)
{
    char line[10];
    FILE *cmd = popen("pidof impressimo", "r");
    fgets(line, 10, cmd);
    pid_t pid = strtoul(line, NULL, 10);
    g_message ("Pid impressimo : %d", (gint)pid);

    return pid;
}

static gboolean
check_impressimo (gpointer data)
{
    pid_t pid = pid_impressimo();
    if (!pid){
        g_warning ("Impressimo stopped");
        g_signal_emit(data, signals[SIGNAL_PROCESS_STOPPED], 0);
        return G_SOURCE_REMOVE;
    }

    g_warning ("Impressimo still running");
    return G_SOURCE_CONTINUE;
}

static void
button_print_cb (GtkToolButton *toolbutton,
                 gpointer       data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(data));

    EogImagesManagerToolbarPrivate *priv = EOG_IMAGES_MANAGER_TOOLBAR(data)->priv;

    GAction *act = g_action_map_lookup_action (G_ACTION_MAP(priv->window), "print");
    gboolean oinfo_exist = g_find_program_in_path ("oinfo") != NULL ? TRUE : FALSE;
    GtkWidget *eog_nav = eog_window_get_thumb_nav(priv->window);
    if (oinfo_exist) {
        if (pid_impressimo () != 0) {
            g_warning ("Impressimo is already running");
            return;
        }

        g_signal_emit(data, signals[SIGNAL_PROCESS_START], 0);

        GString *args = g_string_new ("impressimo -G ");
        GtkTreeModel *model = GTK_TREE_MODEL (eog_window_get_store (priv->window));
        EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV(eog_nav));
        GError *error = NULL;
        EogImage *image;
        GFile *file;
        gchar *path;
        if (mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
            GtkTreeIter iter;
            gboolean valid = gtk_tree_model_get_iter_first(model, &iter);
            if (!valid)
                return;
            do {
                gtk_tree_model_get (model, &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
                file = eog_image_get_file(image);
                path = g_file_get_path(file);
                g_string_append_printf(args, "\"%s\" ", path);
            } while (gtk_tree_model_iter_next(model, &iter));
        } else if (mode == EOG_THUMB_NAV_MODE_ONE_ROW) {
            GtkWidget *eog_view = eog_window_get_thumb_view(priv->window);
            image = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW (eog_view));
            file = eog_image_get_file(image);
            path = g_file_get_path (file);
            g_string_append_printf(args, "\"%s\"", path);
        }
        if (path)
            g_free (path);
        if(file)
            g_object_unref (file);
        if (image)
            g_object_unref (G_OBJECT (image));

        gboolean spawned = g_spawn_command_line_async(args->str, &error);
        if (!spawned || error) {
            g_critical("Could not spawn impressimo : %s", error ? error->message : "Unknown error");
            g_action_activate (act, NULL);
        }
        g_timeout_add (1000, check_impressimo, data);
        g_string_free (args, TRUE);
    }else
        g_action_activate (act, NULL);
    return;
}

static GtkToolItem *
item_button_print (EogImagesManagerToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar), NULL);

    GtkWidget *img_print = gtk_image_new_from_icon_name("printer", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *print = gtk_tool_button_new(img_print, D_("Print"));


    g_signal_connect(GTK_TOOL_BUTTON(print), "clicked", G_CALLBACK(button_print_cb), toolbar);

    return print;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON REMOVE
 *
 * * * * * *  * * * * * * * *  * * */
static GList *
images_in_gallery (EogImagesManagerToolbar *toolbar)
{
    EogImagesManagerToolbarPrivate *priv = toolbar->priv;
    GtkTreeModel *model = GTK_TREE_MODEL(eog_window_get_store(priv->window));

    GtkTreeIter iter;
    gboolean valid = gtk_tree_model_get_iter_first(model, &iter);

    GList *images = NULL;
    while (valid) {
        EogImage *image = NULL;
        gtk_tree_model_get (model, &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);

        GFile *file_image = eog_image_get_file(image);
        images = g_list_prepend(images, file_image);
        valid = gtk_tree_model_iter_next(model, &iter);

        g_object_unref(image);
        g_object_unref(file_image);
    }
    return images;
}

void
remove_images_func (gpointer data,
                    gpointer user_data)
{
    GError *error = NULL;
    gboolean success;
    GFile *file = G_FILE(data);
    g_assert(G_IS_FILE(data));

    // TODO change it to async ?
    success = g_file_trash(file, NULL, &error);
    if (!success || error)
        g_warning("%s : could not remove %s : %s", __func__, g_file_get_parse_name(file), error ? error->message : "Unknwon error");
}

static void
remove_images (EogImagesManagerToolbar *toolbar)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar));

    EogImagesManagerToolbarPrivate *priv = toolbar->priv;

    GList *images = images_in_gallery (toolbar);

    if (images == NULL) {
        g_warning("%s : The list is of images is empty. This is not normal.", __func__);
        return;
    }

    GError *error = NULL;
    GtkWidget *nav_view;
    g_object_get(priv->nav, "thumbview", &nav_view, NULL);
    g_assert (EOG_IS_THUMB_VIEW(nav_view));

    EogImage *image = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW(nav_view));
/*
    GFile *file = eog_image_get_file(image);
    GFile *parent = g_file_get_parent (file);
    gchar *parent_str = g_file_get_uri(parent);
    TrackerSparqlConnection *connection = tracker_sparql_connection_get(NULL, NULL);
    gchar *query = g_strdup_printf("DELETE {\n"
                                   "       ?url a rdfs:Resource\n"
                                   "} WHERE {\n"
                                   "  ?url a nfo:Image ; nfo:belongsToContainer ?folder . FILTER(nie:url(?folder) = '%s') .\n"
                                   "}", parent_str);
    tracker_sparql_connection_update (connection,
                                      query,
                                      G_PRIORITY_DEFAULT,
                                      NULL,
                                      &error);
    if (error) {
         // Some error happened performing the query, not good
    g_printerr("Couldn't update the Tracker store: %s",
               error ? error->message : "unknown error");
    }
 */

    EogListStore *store = EOG_LIST_STORE(gtk_icon_view_get_model(GTK_ICON_VIEW(nav_view)));
    eog_list_store_remove_image(store, image);

    g_list_foreach(images, (GFunc)remove_images_func, NULL);
}

static void
delete_folder (EogImagesManagerToolbar *toolbar)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar));
    EogImagesManagerToolbarPrivate *priv = toolbar->priv;
    GtkWidget *nav_view;
    g_object_get(priv->nav, "thumbview", &nav_view, NULL);
    g_assert (EOG_IS_THUMB_VIEW(nav_view));

    EogImage *image = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW(nav_view));
    GFile *file = g_file_get_parent(eog_image_get_file((image)));
    gchar *path = g_file_get_path(file);

    g_message ("Image : %s", path);

    return;
}

static void
set_valign_center_label (gpointer widget,
                         gpointer data)
{
    if (GTK_IS_LABEL (widget))
        gtk_widget_set_valign(widget, GTK_ALIGN_CENTER);
    return;
}

static void
button_remove_cb (GtkToolButton *toolbutton,
                 gpointer       data)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(data));

    EogImagesManagerToolbarPrivate *priv = EOG_IMAGES_MANAGER_TOOLBAR(data)->priv;

    GtkWidget *align = NULL;
    GtkWidget *box = NULL;
    GList *box_children;

    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(priv->window),GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                               GTK_MESSAGE_QUESTION, GTK_BUTTONS_NONE,
                                               D_("Are you sure you want to delete all images in this folder ?"));
    gtk_widget_set_name (dialog, "dialog-rename");
    gtk_window_set_decorated(GTK_WINDOW(dialog), FALSE);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

    gboolean oinfo_exist = g_find_program_in_path ("oinfo") != NULL ? TRUE : FALSE;
    GtkWidget *img_close = gtk_image_new_from_icon_name("window-close", oinfo_exist? GTK_ICON_SIZE_LARGE_TOOLBAR : GTK_ICON_SIZE_BUTTON);
    GtkWidget *button_close = gtk_button_new_with_label(D_("Cancel"));

    gtk_button_set_image(GTK_BUTTON(button_close), img_close);
    gtk_button_set_always_show_image(GTK_BUTTON(button_close), TRUE);
    gtk_button_set_image_position(GTK_BUTTON(button_close), GTK_POS_LEFT);

    /* Getting label and setting valign before adding the image doesn't apply */
    align = gtk_bin_get_child (GTK_BIN(button_close));
    box = gtk_bin_get_child (GTK_BIN(align));
    box_children = gtk_container_get_children (GTK_CONTAINER (box));
    g_list_foreach(box_children, set_valign_center_label, NULL);
    g_list_free (box_children);

    gtk_dialog_add_action_widget (GTK_DIALOG(dialog), button_close, GTK_RESPONSE_CANCEL);

    GtkWidget *img_ok = gtk_image_new_from_icon_name("gtk-ok", oinfo_exist? GTK_ICON_SIZE_LARGE_TOOLBAR : GTK_ICON_SIZE_BUTTON);
    GtkWidget *button_ok = gtk_button_new_with_label(D_("Validate"));

    gtk_button_set_image(GTK_BUTTON(button_ok), img_ok);
    gtk_button_set_always_show_image(GTK_BUTTON(button_ok), TRUE);
    gtk_button_set_image_position(GTK_BUTTON(button_ok), GTK_POS_LEFT);

    /* Getting label and setting valign before adding the image doesn't apply */
    align = gtk_bin_get_child (GTK_BIN(button_ok));
    box = gtk_bin_get_child (GTK_BIN(align));
    box_children = gtk_container_get_children (GTK_CONTAINER (box));
    g_list_foreach(box_children, set_valign_center_label, NULL);
    g_list_free (box_children);

    gtk_dialog_add_action_widget (GTK_DIALOG(dialog), button_ok, GTK_RESPONSE_OK);

    GtkWidget *bbox = gtk_widget_get_parent(button_ok);
    g_assert (GTK_IS_BUTTON_BOX(bbox));
    gtk_widget_set_halign(bbox, GTK_ALIGN_CENTER);

    /* if the buttons label are not of the same size the icons is not at the same position */
    gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_CENTER);
    gtk_button_box_set_child_non_homogeneous (GTK_BUTTON_BOX (bbox), button_ok, TRUE);
    gtk_button_box_set_child_non_homogeneous (GTK_BUTTON_BOX (bbox), button_close, TRUE);

    gtk_widget_show_all(dialog);

//    GtkTreeModel *model = gtk_icon_view
    int result = gtk_dialog_run(GTK_DIALOG(dialog));
    switch (result){
        case GTK_RESPONSE_OK:
            delete_folder (EOG_IMAGES_MANAGER_TOOLBAR(data));
            //remove_images(EOG_IMAGES_MANAGER_TOOLBAR(data));
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
    return;
}

static GtkToolItem *
item_button_remove (EogImagesManagerToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar), NULL);

    GtkWidget *img_remove = gtk_image_new_from_icon_name("edit-delete", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *remove = gtk_tool_button_new(img_remove, D_("Delete album"));

    g_signal_connect(GTK_TOOL_BUTTON(remove), "clicked", G_CALLBACK(button_remove_cb), toolbar);

    return remove;
}

static void
eog_images_manager_toolbar_init (EogImagesManagerToolbar *m_toolbars)
{
    m_toolbars->priv = eog_images_manager_toolbar_get_instance_private(m_toolbars);
    EogImagesManagerToolbarPrivate *priv = m_toolbars->priv;
//    gtk_stack_set_transition_type(GTK_STACK(stack_toolbars), GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN);

    priv->rename_item = item_button_rename (m_toolbars);
    priv->delete_item = item_button_remove(m_toolbars);

    gtk_toolbar_set_style(GTK_TOOLBAR(m_toolbars), GTK_TOOLBAR_BOTH);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_slideshow (m_toolbars), 0);
    //gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->rename_item, 1);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_print (m_toolbars), 2);
    //gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->delete_item, 3);

    gboolean oinfo_exist = g_find_program_in_path ("oinfo") != NULL ? TRUE : FALSE;

    if (oinfo_exist)
        gtk_toolbar_set_icon_size(GTK_TOOLBAR(m_toolbars), GTK_ICON_SIZE_DIALOG);
}

void
eog_images_manager_toolbar_hide_items(EogImagesManagerToolbar *toolbar, gboolean writable, gboolean hide)
{
    g_return_if_fail(EOG_IS_IMAGES_MANAGER_TOOLBAR(toolbar));

    gtk_widget_set_visible(GTK_WIDGET(toolbar->priv->delete_item), hide);

    if (writable)
        gtk_widget_set_visible(GTK_WIDGET(toolbar->priv->rename_item), hide);
}
