/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_IMAGES_MANAGER_PLUGIN_H
#define EOG_IMAGES_MANAGER_PLUGIN_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-window.h>
#include <libpeas/peas-extension-base.h>
#include <libpeas/peas-object-module.h>

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_IMAGES_MANAGER_PLUGIN		(eog_images_manager_plugin_get_type ())
#define EOG_IMAGES_MANAGER_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_IMAGES_MANAGER_PLUGIN, EogImagesManagerPlugin))
#define EOG_IMAGES_MANAGER_PLUGIN_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_IMAGES_MANAGER_PLUGIN, EogImagesManagerPluginClass))
#define EOG_IS_IMAGES_MANAGER_PLUGIN(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_IMAGES_MANAGER_PLUGIN))
#define EOG_IS_IMAGES_MANAGER_PLUGIN_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_IMAGES_MANAGER_PLUGIN))
#define EOG_IMAGES_MANAGER_PLUGIN_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_IMAGES_MANAGER_PLUGIN, EogImagesManagerPluginClass))

/* Private structure type */
typedef struct _EogImagesManagerPluginPrivate	EogImagesManagerPluginPrivate;

/*
 * Main object structure
 */
typedef struct _EogImagesManagerPlugin		EogImagesManagerPlugin;

struct _EogImagesManagerPlugin
{
    PeasExtensionBase parent_instance;

    EogWindow *window;

    GtkWidget *layout;
    GtkWidget *vbox;
    GtkWidget *hbox;

    GtkWidget *stack_toolbars;

    EogImagesManagerPluginPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogImagesManagerPluginClass	EogImagesManagerPluginClass;

struct _EogImagesManagerPluginClass
{
    PeasExtensionBaseClass parent_class;
};

/*
 * Public methods
 */
GType	eog_images_manager_plugin_get_type		(void) G_GNUC_CONST;

/* All the plugins must implement this function */
G_MODULE_EXPORT void peas_register_types (PeasObjectModule *module);

G_END_DECLS

#endif //EOG_IMAGES_MANAGER_PLUGIN_H
