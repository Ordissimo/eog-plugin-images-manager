/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_IMAGES_MANAGER_NAV_H
#define EOG_IMAGES_MANAGER_NAV_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-window.h>
#include <eog-3.0/eog/eog-thumb-nav.h>

G_BEGIN_DECLS

#define EOG_TYPE_IMAGES_MANAGER_NAV            (eog_images_manager_nav_get_type ())
#define EOG_IMAGES_MANAGER_NAV(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_IMAGES_MANAGER_NAV, EogImagesManagerNav))
#define EOG_IMAGES_MANAGER_NAV_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_IMAGES_MANAGER_NAV, EogImagesManagerNavClass))
#define EOG_IS_IMAGES_MANAGER_NAV(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_IMAGES_MANAGER_NAV))
#define EOG_IS_IMAGES_MANAGER_NAV_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_IMAGES_MANAGER_NAV))
#define EOG_IMAGES_MANAGER_NAV_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_IMAGES_MANAGER_NAV, EogImagesManagerNavClass))

typedef struct _EogImagesManagerNav EogImagesManagerNav;
typedef struct _EogImagesManagerNavClass EogImagesManagerNavClass;
typedef struct _EogImagesManagerNavPrivate EogImagesManagerNavPrivate;

struct _EogImagesManagerNav{
    EogThumbNav base_instance;
    EogImagesManagerNavPrivate *priv;
};

struct _EogImagesManagerNavClass {
    EogThumbNavClass base_class;
    
    void (* images_loading) 	   (EogImagesManagerNav *nav, gboolean set_throbber);
};

GType       eog_images_manager_nav_get_type 		   (void) G_GNUC_CONST;

GtkWidget  *eog_images_manager_nav_new 		       (EogWindow *window,
							GtkWidget *stack,
							GtkWidget *thumb_view,
							gboolean from_eog,
							guint sort,
							const gchar *folder);

GtkWidget  *eog_images_manager_nav_get_thumb_nav       (EogImagesManagerNav *nav);
void        eog_images_manager_nav_fill                (EogImagesManagerNav *nav);
gboolean    eog_images_manager_nav_is_image_in_gallery (GtkTreeModel *gallery_model,
                                                        GFile *file,
                                                        GtkTreeIter *iter_found);

guint       eog_images_manager_nav_get_sort            (EogImagesManagerNav *nav);

void        eog_images_manager_nav_get_images_from_folder (EogImagesManagerNav *nav,
							   guint sort,
							   gchar *name_folder);

void       eog_images_manager_nav_update_label_folder (EogImagesManagerNav *nav);

G_END_DECLS

#endif //EOG_IMAGES_MANAGER_NAV_H
