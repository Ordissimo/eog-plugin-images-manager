/* EogImagesManager - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogImagesManager is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogImagesManager is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a display of image folders.  */

#ifndef EOG_IMAGES_MANAGER_STORE_H
#define EOG_IMAGES_MANAGER_STORE_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-window.h>
#include <eog-3.0/eog/eog-list-store.h>

G_BEGIN_DECLS

#define EOG_TYPE_IMAGES_MANAGER_STORE            (eog_images_manager_store_get_type ())
#define EOG_IMAGES_MANAGER_STORE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_IMAGES_MANAGER_STORE, EogImagesManagerStore))
#define EOG_IMAGES_MANAGER_STORE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_IMAGES_MANAGER_STORE, EogImagesManagerStoreClass))
#define EOG_IS_IMAGES_MANAGER_STORE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_IMAGES_MANAGER_STORE))
#define EOG_IS_IMAGES_MANAGER_STORE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_IMAGES_MANAGER_STORE))
#define EOG_IMAGES_MANAGER_STORE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_IMAGES_MANAGER_STORE, EogImagesManagerStoreClass))

typedef struct _folderContent {
    gchar *path_folder;
    gchar *folder_name;
    gchar *icon_folder;
    gchar *icon_name;
} folderContent;

typedef struct _EogImagesManagerStore EogImagesManagerStore;
typedef struct _EogImagesManagerStoreClass EogImagesManagerStoreClass;
typedef struct _EogImagesManagerStorePrivate EogImagesManagerStorePrivate;

typedef enum {  SORTED_BY_FOLDERS_NAME = 1, SORTED_BY_DATE, SORTED_BY_ALPHABETICAL } EogImagesManagerStoreSort;
enum { /*FOLDER_NAME = EOG_LIST_STORE_NUM_COLUMNS,*/ FOLDER_KEY = EOG_LIST_STORE_NUM_COLUMNS, FOLDER_NUM_COLUMNS };

#if G_ENCODE_VERSION(GLIB_MAJOR_VERSION, GLIB_MINOR_VERSION) < G_ENCODE_VERSION(2, 54)
#define GLIB_VERSION_INF_2_54 1
#else
#define GLIB_VERSION_INF_2_54 0
#endif

struct _EogImagesManagerStore{
    EogListStore base_instance;
    EogImagesManagerStorePrivate *priv;
};

struct _EogImagesManagerStoreClass {
    EogListStoreClass base_class;

    void (*folders_loaded) (GtkWidget *widget, gpointer data);
    void (*folder_updated) (GtkWidget *widget, gpointer data);
};

GType       eog_images_manager_store_get_type 		    (void) G_GNUC_CONST;

GtkListStore  *eog_images_manager_store_new        (EogImagesManagerStoreSort sort);

void           eog_images_manager_store_add_folder (EogImagesManagerStore *store,
                                                    const gchar *path_folder,
                                                    const gchar *foldername,
                                                    const gchar *icon_folder,
                                                    const gchar *icon_name);

void       eog_images_manager_store_remove_folder (EogImagesManagerStore *store);

void      eog_images_manager_store_set_list_store (EogImagesManagerStore *store,
                                                   GSList *list);

G_END_DECLS

#endif //EOG_IMAGES_MANAGER_STORE_H
