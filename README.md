Eog plugin images manager<br>
<br>
Description :<br>
This plugin display in EOG a list of folders containing images retrieved <br>
with libtracker-sparql.  <br>
This list is dispalyed in a column at the left of the application.<br>
They are two types of displaying : <br> 
&nbsp;&nbsp;&nbsp;&nbsp;     - By folders : All folders containing images<br>
&nbsp;&nbsp;&nbsp;&nbsp;     - By dates : We group all images by dates. Note that if the image contain an<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                 Exif date we will display this date. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                 Otherwise the last time it has been modified<br>
                 <br>

```
Requirements :
    For the execution:
         libtracker-sparql >= 2.0
         All requirements from EOG.
        
    For the build:
         meson
         libtracker-control-2.0-dev
         libtracker-sparql-2.0-dev
         libpeas-dev
         eog-dev
```


Install :<br>
To install this plugin :<br>
<br>
```
    meson build 
    cd build
    sudo ninja install
```


License :<br>
This program is released under the terms of the GNU General Public License. <br>
Please see the file LICENSE for details.<br>
<br>